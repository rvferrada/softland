@extends('layout.app')

@section('title')
    Informe comercial
@endsection

@section('body')
    <h3 class="title-so">
        <i class="fa fa-wpforms" aria-hidden="true"></i>
        &nbsp;INFORME COMERCIAL
    </h3>
    <hr>

    <div class="wrapper">
        <!-- INFORME COMERCIAL - GRAU LTDA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">INFORME COMERCIAL - <strong>GRAU LTDA.</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialGR) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAULTDA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialGR as $informeGR)
                                                    @if ($informeGR->FechaMes == $i && $informeGR->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeGR->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualGR as $totalGR)
                                                @if ($totalGR->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalGR->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- GRAU LTDA. Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGR as $TrimestreGR) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreGR->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreGR->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGR as $TrimestreGR)
                                                    @if($TrimestreGR->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreGR->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGR as $TrimestreGR)
                                                    @if($TrimestreGR->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreGR->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGR as $TrimestreGR)
                                                    @if($TrimestreGR->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreGR->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;&nbsp;La empresa <strong>GRAU LTDA.</strong> no posee ventas asociadas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- INFORME COMERCIAL - GRAU SPA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">INFORME COMERCIAL - <strong>GRAU SPA.</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialGS) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialGS as $informeGS)
                                                    @if ($informeGS->FechaMes == $i && $informeGS->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeGS->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualGS as $totalGS)
                                                @if ($totalGS->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalGS->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- GRAU SPA. Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                        <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                        @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                                $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGS as $trimestreGS) {{-- Recorre 4 veces | 0-3 | --}}
                                                @if($trimestreGS->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                                    <?php $valorExiste = true; ?>
                                                    <td>{{ number_format($trimestreGS->PrimerTrimestre, null, ',', '.') }}</td>
                                                @endif
                                            @endforeach

                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                        @endfor
                                    </tr>
                                    <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                        <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                        @for ($i=0;$i<=($anos);$i++)
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGS as $trimestreGS)
                                                @if($trimestreGS->FechaAno == $anoComienzo + $i)
                                                    <?php $valorExiste = true; ?>
                                                    <td>{{ number_format($trimestreGS->SegundoTrimestre, null, ',', '.') }}</td>
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                        @endfor
                                    </tr>
                                    <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                        <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                        @for ($i=0;$i<=($anos);$i++)
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGS as $trimestreGS)
                                                @if($trimestreGS->FechaAno == $anoComienzo + $i)
                                                    <?php $valorExiste = true; ?>
                                                    <td>{{ number_format($trimestreGS->TercerTrimestre, null, ',', '.') }}</td>
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                        @endfor
                                    </tr>
                                    <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                        <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                        @for ($i=0;$i<=($anos);$i++)
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGS as $trimestreGS)
                                                @if($trimestreGS->FechaAno == $anoComienzo + $i)
                                                    <?php $valorExiste = true; ?>
                                                    <td>{{ number_format($trimestreGS->CuartoTrimestre, null, ',', '.') }}</td>
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;&nbsp;La empresa <strong>GRAU SPA.</strong> no posee ventas asociadas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- INFORME COMERCIAL - MICROBOX. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">INFORME COMERCIAL - <strong>MICROBOX</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialMB) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialMB as $informeMB)
                                                    @if ($informeMB->FechaMes == $i && $informeMB->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeMB->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualMB as $totalMB)
                                                @if ($totalMB->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalMB->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- MICROBOX Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreMB as $TrimestreMB) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreMB->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreMB->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreMB as $TrimestreMB)
                                                    @if($TrimestreMB->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreMB->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreMB as $TrimestreMB)
                                                    @if($TrimestreMB->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreMB->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreMB as $TrimestreMB)
                                                    @if($TrimestreMB->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreMB->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;La empresa <strong>MICROBOX</strong> no posee ventas asociadas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- INFORME COMERCIAL - TENDENCIA LTDA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">INFORME COMERCIAL - <strong>TENDENCIA LTDA.</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialTL) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialTL as $informeTL)
                                                    @if ($informeTL->FechaMes == $i && $informeTL->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeTL->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualTL as $totalTL)
                                                @if ($totalTL->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalTL->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- TENDENCIA LTDA. Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreTL as $TrimestreTL) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreTL->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreTL->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTL as $TrimestreTL)
                                                    @if($TrimestreTL->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTL->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTL as $TrimestreTL)
                                                    @if($TrimestreTL->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTL->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTL as $TrimestreTL)
                                                    @if($TrimestreTL->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTL->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;&nbsp;La empresa <strong>TENDENCIA LTDA.</strong> no posee ventas asociadas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- INFORME COMERCIAL - TENDENCIA SPA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">INFORME COMERCIAL - <strong>TENDENCIA SPA.</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialTS) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialTS as $informeTS)
                                                    @if ($informeTS->FechaMes == $i && $informeTS->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeTS->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualTS as $totalTS)
                                                @if ($totalTS->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalTS->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- TENDENCIA SPA Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreTS as $TrimestreTS) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreTS->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreTS->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTS as $TrimestreTS)
                                                    @if($TrimestreTS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTS->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTS as $TrimestreTS)
                                                    @if($TrimestreTS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTS->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTS as $TrimestreTS)
                                                    @if($TrimestreTS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTS->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;&nbsp;La empresa <strong>TENDENCIA SPA.</strong> no posee ventas asociadas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection