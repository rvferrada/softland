@extends('layout.app')

@section('title')
    Informe de vendedores
@endsection

@section('body')
    <h3 class="title-so">
        <i class="fa fa-wpforms" aria-hidden="true"></i>
        &nbsp;Informe de vendedores
    </h3>
    <hr>

    <!-- Standard button -->
    <a class="btn btn-default no-print" href="{{ url('vendedores') }}" role="button">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        &nbsp;Volver al buscador
    </a>

    <div class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th scope="row" style="width: 200px">Nombre</th>
                            <td>{{ $nombre }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Fecha de informe</th>
                            <td>{{ $fecha }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <hr>

        <!-- Información de ventas. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Información de ventas (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>Grau Ltda</th>
                                        <th>Grau Spa</th>
                                        <th>Microbox</th>
                                        <th>Tendencia Ltda</th>
                                        <th>Tendencia Spa</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Total facturado (Hace 2 Meses)</th>
                                        <?php $totalEmpresas = 0 ?>
                                        @foreach ($totalFacturadoHaceDosMeses as $facturasDosMeses)
                                            <?php $totalEmpresas = $facturasDosMeses->total + $totalEmpresas ?>
                                            <td>{{ number_format($facturasDosMeses->total, null, ',', '.') }}</td>
    
                                            @if ($facturasDosMeses->empresa == "GRAULTDA")
                                                <?php $totalAnoActualGrauLtda = $facturasDosMeses->total ?>
                                            @elseif ($facturasDosMeses->empresa == "GRAUSPA")
                                                <?php $totalAnoActualGrauSpa = $facturasDosMeses->total ?>
                                            @elseif ($facturasDosMeses->empresa == "MICROBOX")
                                                <?php $totalAnoActualMicrobox = $facturasDosMeses->total ?>
                                            @elseif ($facturasDosMeses->empresa == "PUBLIGRAFIKA")
                                                <?php $totalAnoActualTenLtda = $facturasDosMeses->total ?>
                                            @elseif ($facturasDosMeses->empresa == "TENSPA")
                                                <?php $totalAnoActualTenSpa = $facturasDosMeses->total ?>
                                            @endif
    
                                        @endforeach
                                        <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total facturado (Hace 1 Mes)</th>
                                        <?php $totalEmpresas = 0 ?>
                                        @foreach ($totalFacturadoHaceUnMes as $facturadoUnMes)
                                            <?php $totalEmpresas = $facturadoUnMes->total + $totalEmpresas ?>
                                            <td>{{ number_format($facturadoUnMes->total, null, ',', '.') }}</td>
    
                                            @if ($facturadoUnMes->empresa == "GRAULTDA")
                                                <?php $totalAnoAnteriorGrauLtda = $facturadoUnMes->total ?>
                                            @elseif ($facturadoUnMes->empresa == "GRAUSPA")
                                                <?php $totalAnoAnteriorGrauSpa = $facturadoUnMes->total ?>
                                            @elseif ($facturadoUnMes->empresa == "MICROBOX")
                                                <?php $totalAnoAnteriorMicrobox = $facturadoUnMes->total ?>
                                            @elseif ($facturadoUnMes->empresa == "PUBLIGRAFIKA")
                                                <?php $totalAnoAnteriorTenLtda = $facturadoUnMes->total ?>
                                            @elseif ($facturadoUnMes->empresa == "TENSPA")
                                                <?php $totalAnoAnteriorTenSpa = $facturadoUnMes->total ?>
                                            @endif
                                        @endforeach
                                        <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total facturado (Mes actual)</th>
                                        <?php $totalEmpresas = 0 ?>
                                        @foreach ($totalFacturadoActual as $facturadoActual)
                                            <?php $totalEmpresas = $facturadoActual->Total + $totalEmpresas ?>
                                            <td>{{ number_format($facturadoActual->Total, null, ',', '.') }}</td>

                                            @if ($facturadoActual->empresa == "GRAULTDA")
                                                <?php $totalAnoAnteriorGrauLtda = $facturadoActual->Total ?>
                                            @elseif ($facturadoActual->empresa == "GRAUSPA")
                                                <?php $totalAnoAnteriorGrauSpa = $facturadoActual->Total ?>
                                            @elseif ($facturadoActual->empresa == "MICROBOX")
                                                <?php $totalAnoAnteriorMicrobox = $facturadoActual->Total ?>
                                            @elseif ($facturadoActual->empresa == "PUBLIGRAFIKA")
                                                <?php $totalAnoAnteriorTenLtda = $facturadoActual->Total ?>
                                            @elseif ($facturadoActual->empresa == "TENSPA")
                                                <?php $totalAnoAnteriorTenSpa = $facturadoActual->Total ?>
                                            @endif
                                        @endforeach
                                        <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Información de cobros (pagos) y atrasos. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Información de cobros y atrasos (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Grau Ltda</th>
                                    <th>Grau Spa</th>
                                    <th>Microbox</th>
                                    <th>Tendencia Ltda</th>
                                    <th>Tendencia Spa</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">Total cobrado (Hace 1 mes)</th>
                                        <?php $totalEmpresas = 0 ?>
                                        @foreach ($totalCobradoHaceUnMes as $cobradoHaceUnMes)
                                            <?php $totalEmpresas = $cobradoHaceUnMes->Abonos + $totalEmpresas ?>
                                            <td>{{ number_format($cobradoHaceUnMes->Abonos, null, ',', '.') }}</td>

                                            @if ($cobradoHaceUnMes->empresa == "GRAULTDA")
                                                <?php $totalAnoActualGrauLtda = $cobradoHaceUnMes->Abonos ?>
                                            @elseif ($cobradoHaceUnMes->empresa == "GRAUSPA")
                                                <?php $totalAnoActualGrauSpa = $cobradoHaceUnMes->Abonos ?>
                                            @elseif ($cobradoHaceUnMes->empresa == "MICROBOX")
                                                <?php $totalAnoActualMicrobox = $cobradoHaceUnMes->Abonos ?>
                                            @elseif ($cobradoHaceUnMes->empresa == "PUBLIGRAFIKA")
                                                <?php $totalAnoActualTenLtda = $cobradoHaceUnMes->Abonos ?>
                                            @elseif ($cobradoHaceUnMes->empresa == "TENSPA")
                                                <?php $totalAnoActualTenSpa = $cobradoHaceUnMes->Abonos ?>
                                            @endif

                                        @endforeach
                                        <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total cobrado (Mes actual)</th>
                                        <?php $totalEmpresas = 0 ?>
                                        @foreach ($totalCobradoMesActual as $cobradoMesActual)
                                            <?php $totalEmpresas = $cobradoMesActual->Abonos + $totalEmpresas ?>
                                            <td>{{ number_format($cobradoMesActual->Abonos, null, ',', '.') }}</td>

                                            @if ($cobradoMesActual->empresa == "GRAULTDA")
                                                <?php $totalAnoActualGrauLtda = $cobradoMesActual->Abonos ?>
                                            @elseif ($cobradoMesActual->empresa == "GRAUSPA")
                                                <?php $totalAnoActualGrauSpa = $cobradoMesActual->Abonos ?>
                                            @elseif ($cobradoMesActual->empresa == "MICROBOX")
                                                <?php $totalAnoActualMicrobox = $cobradoMesActual->Abonos ?>
                                            @elseif ($cobradoMesActual->empresa == "PUBLIGRAFIKA")
                                                <?php $totalAnoActualTenLtda = $cobradoMesActual->Abonos ?>
                                            @elseif ($cobradoMesActual->empresa == "TENSPA")
                                                <?php $totalAnoActualTenSpa = $cobradoMesActual->Abonos ?>
                                            @endif

                                        @endforeach
                                        <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total vencido</th>
                                        <td>
                                            <?php
                                            $totalEmpresaGRvencido = 0;
                                            $totalEmpresaGSvencido = 0;
                                            $totalEmpresaMBvencido = 0;
                                            $totalEmpresaTLvencido = 0;
                                            $totalEmpresaTSvencido = 0;
                                            $valorExiste = false
                                            ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'GRAULTDA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaGRvencido = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'GRAUSPA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaGSvencido = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'MICROBOX' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaMBvencido = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'PUBLIGRAFIKA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaTLvencido = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'TENSPA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaTSvencido = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php
                                            $totalVencidoEmpresas =
                                                $totalEmpresaGRvencido +
                                                $totalEmpresaGSvencido +
                                                $totalEmpresaMBvencido +
                                                $totalEmpresaTLvencido +
                                                $totalEmpresaTSvencido;
                                            ?>
                                            {{ number_format($totalVencidoEmpresas, null, ',', '.') }}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Total por vencer</th>
                                        <td>
                                            <?php
                                            $totalEmpresaGRporVencer = 0;
                                            $totalEmpresaGSporVencer = 0;
                                            $totalEmpresaMBporVencer = 0;
                                            $totalEmpresaTLporVencer = 0;
                                            $totalEmpresaTSporVencer = 0;
                                            $valorExiste = false
                                            ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'GRAULTDA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaGRporVencer = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'GRAUSPA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaGSporVencer = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'MICROBOX' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaMBporVencer = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'PUBLIGRAFIKA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaTLporVencer = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php $valorExiste = false ?>
                                            @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                                @if($totalVencidoYporVencerEmpresa->empresa == 'TENSPA' &&
                                                $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                    <?php
                                                    $valorExiste = true;
                                                    $totalEmpresaTSporVencer = $totalVencidoYporVencerEmpresa->Total;
                                                    ?>
                                                    {{ number_format($totalVencidoYporVencerEmpresa->Total, null, ',', '.') }}
                                                @endif
                                            @endforeach

                                            @if(!$valorExiste)
                                                {{ number_format(0, null, ',', '.') }}
                                            @endif
                                        </td>
                                        <td>
                                            <?php
                                            $totalPorVencerEmpresas =
                                                $totalEmpresaGRporVencer +
                                                $totalEmpresaGSporVencer +
                                                $totalEmpresaMBporVencer +
                                                $totalEmpresaTLporVencer +
                                                $totalEmpresaTSporVencer;
                                            ?>
                                            {{ number_format($totalPorVencerEmpresas, null, ',', '.') }}
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Informe comercial - GRAU LTDA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Informe comercial - <strong>GRAU LTDA</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialGR) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        @for($i=1; $i<=12; $i++)
                                            <tr @if($i == date('n')) class="info" @endif>
                                                <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                                {{-- GRAULTDA --}}
                                                <?php
                                                    $valorExiste = false
                                                ?>
                                                @for($j = 0; $j<=$anos; $j++)
                                                    @foreach($informeComercialGR as $informeGR)
                                                        @if ($informeGR->FechaMes == $i && $informeGR->FechaAno == ($anoComienzo + $j))
                                                            <td>{{ number_format($informeGR->Total, null, ',', '.') }}</td>
                                                            <?php
                                                                $valorExiste = true;
                                                            ?>
                                                        @endif
                                                    @endforeach

                                                    @if (!$valorExiste)
                                                        <td>{{ number_format(0, null, ',', '.') }}</td>
                                                    @endif

                                                    <?php $valorExiste = false; ?>
                                                @endfor
                                            </tr>
                                        @endfor
                                        <tr>
                                            @for($i = 0; $i<=($anos+1); $i++)
                                                <td><span class="text-center-tb-so">-</span></td>
                                            @endfor
                                        </tr>
                                        <tr>
                                            <th scope="row">Total anual</th>
                                            <?php $valorExiste = false; ?>
                                            @for($i = 0; $i<=($anos); $i++)
                                                @foreach ($totalAnualGR as $totalGR)
                                                    @if ($totalGR->FechaAno == ($anoComienzo + $i))
                                                        <th scope="row">{{ number_format($totalGR->Total, null, ',', '.') }}</th>
                                                        <?php
                                                            $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- GRAU LTDA. Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGR as $TrimestreGR) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreGR->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreGR->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGR as $TrimestreGR)
                                                    @if($TrimestreGR->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreGR->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGR as $TrimestreGR)
                                                    @if($TrimestreGR->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreGR->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGR as $TrimestreGR)
                                                    @if($TrimestreGR->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreGR->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El vendedor <strong>{{ $nombre }}</strong> no posee ventas en <strong>GRAU LTDA.</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Informe comercial - GRAU SPA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Informe comercial - <strong>GSAU SPA</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialGS) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialGS as $informeGS)
                                                    @if ($informeGS->FechaMes == $i && $informeGS->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeGS->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualGS as $totalGS)
                                                @if ($totalGS->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalGS->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- GRAU SPA. Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreGS as $trimestreGS) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($trimestreGS->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($trimestreGS->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGS as $trimestreGS)
                                                    @if($trimestreGS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($trimestreGS->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGS as $trimestreGS)
                                                    @if($trimestreGS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($trimestreGS->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreGS as $trimestreGS)
                                                    @if($trimestreGS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($trimestreGS->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El vendedor <strong>{{ $nombre }}</strong> no posee ventas en <strong>GRAU SPA.</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
        <hr>

        <!-- Informe comercial - MICROBOX. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Informe comercial - <strong>MICROBOX</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialMB) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialMB as $informeMB)
                                                    @if ($informeMB->FechaMes == $i && $informeMB->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeMB->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualMB as $totalMB)
                                                @if ($totalMB->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalMB->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- MICROBOX Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreMB as $TrimestreMB) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreMB->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreMB->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreMB as $TrimestreMB)
                                                    @if($TrimestreMB->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreMB->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreMB as $TrimestreMB)
                                                    @if($TrimestreMB->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreMB->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreMB as $TrimestreMB)
                                                    @if($TrimestreMB->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreMB->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El vendedor <strong>{{ $nombre }}</strong> no posee ventas en <strong>MICROBOX.</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Informe comercial - TENDENCIA LTDA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Informe comercial - <strong>TENDENCIA LTDA</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialTL) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialTL as $informeTL)
                                                    @if ($informeTL->FechaMes == $i && $informeTL->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeTL->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualTL as $totalTL)
                                                @if ($totalTL->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalTL->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- TENDENCIA LTDA. Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreTL as $TrimestreTL) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreTL->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreTL->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTL as $TrimestreTL)
                                                    @if($TrimestreTL->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTL->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTL as $TrimestreTL)
                                                    @if($TrimestreTL->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTL->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTL as $TrimestreTL)
                                                    @if($TrimestreTL->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTL->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El vendedor <strong>{{ $nombre }}</strong> no posee ventas en <strong>TENDENCIA LTDA.</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        
        <hr>

        <!-- Informe comercial - TENDENCIA SPA. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Informe comercial - <strong>TENDENCIA SPA</strong> (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($informeComercialTS) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Mes</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @for($i=1; $i<=12; $i++)
                                        <tr @if($i == date('n')) class="info" @endif>
                                            <th scope="row">{{ $meses[$i] }} @if($i == date('n'))&nbsp;<span class="label label-info">Mes actual</span> @endif</th>
                                            {{-- GRAUSPA --}}
                                            <?php
                                            $valorExiste = false
                                            ?>
                                            @for($j = 0; $j<=$anos; $j++)
                                                @foreach($informeComercialTS as $informeTS)
                                                    @if ($informeTS->FechaMes == $i && $informeTS->FechaAno == ($anoComienzo + $j))
                                                        <td>{{ number_format($informeTS->Total, null, ',', '.') }}</td>
                                                        <?php
                                                        $valorExiste = true;
                                                        ?>
                                                    @endif
                                                @endforeach

                                                @if (!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif

                                                <?php $valorExiste = false; ?>
                                            @endfor
                                        </tr>
                                    @endfor
                                    <tr>
                                        @for($i = 0; $i<=($anos+1); $i++)
                                            <td><span class="text-center-tb-so">-</span></td>
                                        @endfor
                                    </tr>
                                    <tr>
                                        <th scope="row">Total anual</th>
                                        <?php $valorExiste = false; ?>
                                        @for($i = 0; $i<=($anos); $i++)
                                            @foreach ($totalAnualTS as $totalTS)
                                                @if ($totalTS->FechaAno == ($anoComienzo + $i))
                                                    <th scope="row">{{ number_format($totalTS->Total, null, ',', '.') }}</th>
                                                    <?php
                                                    $valorExiste = true;
                                                    ?>
                                                @endif
                                            @endforeach

                                            @if (!$valorExiste)
                                                <th scope="row">{{ number_format(0, null, ',', '.') }}</th>
                                            @endif

                                            <?php $valorExiste = false; ?>
                                        @endfor
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <hr>

                            <!-- TENDENCIA SPA Trimestre -->
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Trimestre</th>
                                        @for($i = 0; $i<=$anos; $i++)
                                            <th>{{ ($anoComienzo + $i) }}</th>
                                        @endfor
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr @if(date('n') == 1 || date('n') == 2 || date('n') == 3) class="info" @endif>
                                            <th scope="row">1° trimestre @if(date('n') == 1 || date('n') == 2 || date('n') == 3) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++) {{-- 5 veces si o si relleno --}}
                                            <?php
                                            $valorExiste = false;
                                            ?>
                                            @foreach($totalTrimestreTS as $TrimestreTS) {{-- Recorre 4 veces | 0-3 | --}}
                                            @if($TrimestreTS->FechaAno == $anoComienzo + $i) {{-- $i suma anos 2013, 2014... --}}
                                            <?php $valorExiste = true; ?>
                                            <td>{{ number_format($TrimestreTS->PrimerTrimestre, null, ',', '.') }}</td>
                                            @endif
                                            @endforeach
    
                                            {{-- Comprobamos si encuentra el año que busco. --}}
                                            @if(!$valorExiste)
                                                <td>{{ number_format(0, null, ',', '.') }}</td>
                                            @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 4 || date('n') == 5 || date('n') == 6) class="info" @endif >
                                            <th scope="row">2° trimestre @if(date('n') == 4 || date('n') == 5 || date('n') == 6) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTS as $TrimestreTS)
                                                    @if($TrimestreTS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTS->SegundoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 7 || date('n') == 8 || date('n') == 9) class="info" @endif>
                                            <th scope="row">3° trimestre @if(date('n') == 7 || date('n') == 8 || date('n') == 9) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTS as $TrimestreTS)
                                                    @if($TrimestreTS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTS->TercerTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                        <tr @if(date('n') == 10 || date('n') == 11 || date('n') == 12) class="info" @endif>
                                            <th scope="row">4° trimestre @if(date('n') == 10 || date('n') == 11 || date('n') == 12) &nbsp;<span class="label label-info">Trimestre actual</span> @endif</th>
                                            @for ($i=0;$i<=($anos);$i++)
                                                <?php
                                                $valorExiste = false;
                                                ?>
                                                @foreach($totalTrimestreTS as $TrimestreTS)
                                                    @if($TrimestreTS->FechaAno == $anoComienzo + $i)
                                                        <?php $valorExiste = true; ?>
                                                        <td>{{ number_format($TrimestreTS->CuartoTrimestre, null, ',', '.') }}</td>
                                                    @endif
                                                @endforeach
    
                                                @if(!$valorExiste)
                                                    <td>{{ number_format(0, null, ',', '.') }}</td>
                                                @endif
                                            @endfor
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El vendedor <strong>{{ $nombre }}</strong> no posee ventas en <strong>TENDENCIA SPA.</strong>
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>        

    </div>
@endsection