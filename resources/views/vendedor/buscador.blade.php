@extends('layout.app')

@section('title')
    Informe de vendedores
@endsection

@section('style')
    <link href="{{ asset('bootstrap-select-1.12.2/css/bootstrap-select.min.css') }}" rel="stylesheet">
@endsection

@section('body')
    <h3 class="title-so">
        <i class="fa fa-wpforms" aria-hidden="true"></i>
        &nbsp;Informe de vendedores
    </h3>
    <hr>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <form action="v" method="GET">
                    <div class="panel-body">

                        <!-- Info -->
                        @include('layout.alert')

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="nomVend">Nombre del vendedor: </label>
                                    <select id="nomVend" name="nomVend" class="form-control selectpicker" data-live-search="true" title="Seleccione el vendedor aquí..." data-size="10">
                                        @foreach($vendedores as $vendedor)
                                            <option value="{{ $vendedor->VenDes }}">{{ $vendedor->VenDes }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-line-chart" aria-hidden="true"></i>
                                Generar informe
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('bootstrap-select-1.12.2/js/bootstrap-select.min.js') }}"></script>
@endsection