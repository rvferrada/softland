<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>..:: Grau ::.. @yield('title')</title>

    <!-- Bootstrap -->
    <link href="bootstrap-3.3.7/css/bootstrap.css" rel="stylesheet">

    <!-- Core softland -->
    <link rel="stylesheet" type="text/css" href="core-1.0/core.css">

    <!-- Font awesome -->
    <link rel="stylesheet" href="font-awesome-4.7.0/css/font-awesome.min.css">

    <!-- Roboto font -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">

    @yield('style')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
    <nav class="navbar navbar-inverse navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="collapsed navbar-toggle" data-toggle="collapse" aria-expanded="false" data-target="#myNavbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a href="{{ url('/') }}" class="navbar-brand">
                    <i class="fa fa-line-chart" aria-hidden="true"></i>
                    &nbsp;Softland E.R.P.
                </a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="nav navbar-nav">
                    <li {{ (Request::is('/') || Request::is('c') ? 'class=active' : '') }}><a href="{{ url('/') }}">Clientes</a></li>
                    <li {{ (Request::is('v') || Request::is('vendedores') ? 'class=active' : '') }}><a href="{{ url('vendedores') }}">Vendedores</a></li>
                    <li {{ (Request::is('ega') ? 'class=active' : '') }}><a href="{{ url('ega') }}">EGA</a></li>
                    <li {{ (Request::is('comercial') ? 'class=active' : '') }}><a href="{{ url('comercial') }}">informe Comercial</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
        @yield('body')
    </div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('bootstrap-3.3.7/js/bootstrap.min.js') }}"></script>

@yield('script')

</body>
</html>
