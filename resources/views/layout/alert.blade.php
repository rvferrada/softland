{{-- Errores. --}}
@if (count($errors) > 0)
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-danger">
                <strong>!Oh error!</strong>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
@endif

@if (isset($info) && !empty($info))
    {{-- Info --}}
    <div class="row">
        <div class="col-sm-12">
            <div class="alert alert-info">
                <i class="fa fa-info-circle" aria-hidden="true"></i>
                &nbsp;{{ $info }}
            </div>
        </div>
    </div>
@endif