@extends('layout.app')

@section('title')
    Informe de clientes
@endsection

@section('style')
    <link href="{{ asset('autocomplete/jquery.autocomplete.css') }}" rel="stylesheet">
@endsection

@section('body')
    <h3 class="title-so">
        <i class="fa fa-wpforms" aria-hidden="true"></i>
        &nbsp;Informe de clientes
    </h3>
    <hr>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="panel panel-default">
                <form action="c" method="GET">
                    <div class="panel-body">

                        <!-- Info -->
                        @include('layout.alert')

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="row">
                                    <div class="col-sm-4" style="margin-right: 0px">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                                </div>
                                                <input type="text" name="rut" id="autocompleteRut" class="form-control" placeholder="Rut del cliente">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <i class="fa fa-user-circle-o" aria-hidden="true"></i>
                                                </div>
                                                <input type="text" name="nombre" id="autocompleteNombre" class="form-control" placeholder="Nombre del cliente">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">
                                <i class="fa fa-line-chart" aria-hidden="true"></i>
                                Generar informe
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('autocomplete/jquery.autocomplete.js') }}"></script>

    <script type="text/javascript">
        $('#autocompleteNombre').autocomplete({
            serviceUrl: 'b/nombre',
            onSelect: function (suggestion) {
                $("input[name='rut']").val(suggestion.data)
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: 'El nombre del cliente no existe.'
        });

        $('#autocompleteRut').autocomplete({
            serviceUrl: 'b/rut',
            onSelect: function (suggestion) {
                $("input[name='nombre']").val(suggestion.data)
            },
            showNoSuggestionNotice: true,
            noSuggestionNotice: 'El rut del cliente no existe.'
        });
    </script>
@endsection