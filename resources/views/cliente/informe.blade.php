@extends('layout.app')

@section('title')
    Informe de clientes
@endsection

@section('body')
    <h3 class="title-so">
        <i class="fa fa-wpforms" aria-hidden="true"></i>
        &nbsp;Informe de clientes
    </h3>
    <hr>

    <!-- Standard button -->
    <a class="btn btn-default no-print" href="{{ url('/') }}" role="button">
        <i class="fa fa-chevron-left" aria-hidden="true"></i>
        &nbsp;Volver al buscador
    </a>

    <div class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th scope="row" style="width: 200px">Nombre</th>
                            <td>{{ $nombre }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Rut</th>
                            <td>{{ $rut }}</td>
                        </tr>
                        <tr>
                            <th scope="row">Fecha de informe</th>
                            <td>{{ $fecha }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <hr>

        <!-- Información de ventas. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Información de ventas (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Grau Ltda</th>
                                    <th>Grau Spa</th>
                                    <th>Microbox</th>
                                    <th>Tendencia Ltda</th>
                                    <th>Tendencia Spa</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Total comprado ({{ $ano }})</th>
                                    <?php $totalEmpresas = 0 ?>
                                    @foreach ($totalAnoActual as $totalObj)
                                        <?php $totalEmpresas = $totalObj->total + $totalEmpresas ?>
                                        <td>{{ number_format($totalObj->total, null, ',', '.') }}</td>

                                        @if ($totalObj->empresa == "GRAULTDA")
                                            <?php $totalAnoActualGrauLtda = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "GRAUSPA")
                                            <?php $totalAnoActualGrauSpa = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "MICROBOX")
                                            <?php $totalAnoActualMicrobox = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "PUBLIGRAFIKA")
                                            <?php $totalAnoActualTenLtda = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "TENSPA")
                                            <?php $totalAnoActualTenSpa = $totalObj->total ?>
                                        @endif

                                    @endforeach
                                    <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Total comprado ({{ $ano - 1 }})</th>
                                    <?php $totalEmpresas = 0 ?>
                                    @foreach ($totalAnoAnterior as $totalObj)
                                        <?php $totalEmpresas = $totalObj->total + $totalEmpresas ?>
                                        <td>{{ number_format($totalObj->total, null, ',', '.') }}</td>

                                        @if ($totalObj->empresa == "GRAULTDA")
                                            <?php $totalAnoAnteriorGrauLtda = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "GRAUSPA")
                                            <?php $totalAnoAnteriorGrauSpa = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "MICROBOX")
                                            <?php $totalAnoAnteriorMicrobox = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "PUBLIGRAFIKA")
                                            <?php $totalAnoAnteriorTenLtda = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "TENSPA")
                                            <?php $totalAnoAnteriorTenSpa = $totalObj->total ?>
                                        @endif
                                    @endforeach
                                    <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Total vencido</th>
                                    <td>
                                        <?php
                                        $totalEmpresaGRvencido = 0;
                                        $totalEmpresaGSvencido = 0;
                                        $totalEmpresaMBvencido = 0;
                                        $totalEmpresaTLvencido = 0;
                                        $totalEmpresaTSvencido = 0;
                                        $valorExiste = false
                                        ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'GRAULTDA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaGRvencido = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'GRAUSPA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaGSvencido = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'MICROBOX' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaMBvencido = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'PUBLIGRAFIKA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaTLvencido = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'TENSPA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'VENCIDO')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaTSvencido = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                        $totalVencidoEmpresas =
                                            $totalEmpresaGRvencido +
                                            $totalEmpresaGSvencido +
                                            $totalEmpresaMBvencido +
                                            $totalEmpresaTLvencido +
                                            $totalEmpresaTSvencido;
                                        ?>
                                        {{ number_format($totalVencidoEmpresas, null, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Total por vencer</th>
                                    <td>
                                        <?php
                                        $totalEmpresaGRporVencer = 0;
                                        $totalEmpresaGSporVencer = 0;
                                        $totalEmpresaMBporVencer = 0;
                                        $totalEmpresaTLporVencer = 0;
                                        $totalEmpresaTSporVencer = 0;
                                        $valorExiste = false
                                        ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'GRAULTDA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaGRporVencer = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'GRAUSPA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaGSporVencer = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'MICROBOX' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaMBporVencer = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'PUBLIGRAFIKA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaTLporVencer = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach($totalVencidoYporVencer as $totalVencidoYporVencerEmpresa)
                                            @if($totalVencidoYporVencerEmpresa->empresa == 'TENSPA' &&
                                            $totalVencidoYporVencerEmpresa->estadoFactura == 'POR VENCER')
                                                <?php
                                                $valorExiste = true;
                                                $totalEmpresaTSporVencer = $totalVencidoYporVencerEmpresa->total;
                                                ?>
                                                {{ number_format($totalVencidoYporVencerEmpresa->total, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            {{ number_format(0, null, ',', '.') }}
                                        @endif
                                    </td>
                                    <td>
                                        <?php
                                        $totalPorVencerEmpresas =
                                            $totalEmpresaGRporVencer +
                                            $totalEmpresaGSporVencer +
                                            $totalEmpresaMBporVencer +
                                            $totalEmpresaTLporVencer +
                                            $totalEmpresaTSporVencer;
                                        ?>
                                        {{ number_format($totalPorVencerEmpresas, null, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Facturas en cartera</th>
                                    <td>
                                        <?php
                                        $totalEnCarteraGR = $totalEmpresaGRvencido + $totalEmpresaGRporVencer;
                                        ?>
                                        {{ number_format($totalEnCarteraGR, null, ',', '.') }}
                                    </td>
                                    <td>
                                        <?php
                                        $totalEnCarteraGS = $totalEmpresaGSvencido + $totalEmpresaGSporVencer;
                                        ?>
                                        {{ number_format($totalEnCarteraGS, null, ',', '.') }}
                                    </td>
                                    <td>
                                        <?php
                                        $totalEnCarteraMB = $totalEmpresaMBvencido + $totalEmpresaMBporVencer;
                                        ?>
                                        {{ number_format($totalEnCarteraMB, null, ',', '.') }}
                                    </td>
                                    <td>
                                        <?php
                                        $totalEnCarteraTL = $totalEmpresaTLvencido + $totalEmpresaTLporVencer;
                                        ?>
                                        {{ number_format($totalEnCarteraTL, null, ',', '.') }}
                                    </td>
                                    <td>
                                        <?php
                                        $totalEnCarteraTS = $totalEmpresaTSvencido + $totalEmpresaTSporVencer;
                                        ?>
                                        {{ number_format($totalEnCarteraTS, null, ',', '.') }}
                                    </td>
                                    <td>
                                        <?php
                                        $totalEnCartera = $totalVencidoEmpresas + $totalPorVencerEmpresas;
                                        ?>
                                        {{ number_format($totalEnCartera, null, ',', '.') }}
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Facturas en factoring</th>
                                    <?php $totalFactoringEmpresas = 0 ?>
                                    @foreach ($facturasEnFactoring as $facturaEnFactoring)
                                        <?php $totalFactoringEmpresas = $facturaEnFactoring->total + $totalFactoringEmpresas ?>
                                        <td>{{ number_format($facturaEnFactoring->total, null, ',', '.') }}</td>

                                        @if ($facturaEnFactoring->empresa == "GRAULTDA")
                                            <?php $totalFactoringGrauLtda = $facturaEnFactoring->total ?>
                                        @elseif ($facturaEnFactoring->empresa == "GRAUSPA")
                                            <?php $totalFactoringGrauSpa = $facturaEnFactoring->total ?>
                                        @elseif ($facturaEnFactoring->empresa == "MICROBOX")
                                            <?php $totalFactoringMicrobox = $facturaEnFactoring->total ?>
                                        @elseif ($facturaEnFactoring->empresa == "PUBLIGRAFIKA")
                                            <?php $totalFactoringTenLtda = $facturaEnFactoring->total ?>
                                        @elseif ($facturaEnFactoring->empresa == "TENSPA")
                                            <?php $totalFactoringTenSpa = $facturaEnFactoring->total ?>
                                        @endif
                                    @endforeach
                                    <td>{{ number_format($totalFactoringEmpresas, null, ',', '.') }}</td>
                                </tr>
                                <tr>
                                    <th scope="row">Total general</th>
                                    <?php
                                    $totalGeneralGr = ($totalAnoActualGrauLtda + $totalAnoAnteriorGrauLtda + $totalEmpresaGRvencido + $totalEmpresaGRporVencer + $totalFactoringGrauLtda);
                                    $totalGeneralGs = ($totalAnoActualGrauSpa + $totalAnoAnteriorGrauSpa + $totalEmpresaGSvencido + $totalEmpresaGSporVencer + $totalFactoringGrauSpa);
                                    $totalGeneralMb = ($totalAnoActualMicrobox + $totalAnoAnteriorMicrobox + $totalEmpresaMBvencido + $totalEmpresaMBporVencer + $totalFactoringMicrobox);
                                    $totalGeneralTl = ($totalAnoActualTenLtda + $totalAnoAnteriorTenLtda + $totalEmpresaTLvencido + $totalEmpresaTLporVencer + $totalFactoringTenLtda);
                                    $totalGeneralTs = ($totalAnoActualTenSpa + $totalAnoAnteriorTenSpa + $totalEmpresaTSvencido + $totalEmpresaTSporVencer + $totalFactoringTenSpa);
                                    $totalGeneralFinal = ($totalGeneralGr + $totalGeneralGs + $totalGeneralMb + $totalGeneralTl + $totalGeneralTs);
                                    ?>
                                    <td>{{ number_format($totalGeneralGr, null, ',', '.') }}</td>
                                    <td>{{ number_format($totalGeneralGs, null, ',', '.') }}</td>
                                    <td>{{ number_format($totalGeneralMb, null, ',', '.') }}</td>
                                    <td>{{ number_format($totalGeneralTl, null, ',', '.') }}</td>
                                    <td>{{ number_format($totalGeneralTs, null, ',', '.') }}</td>
                                    <td>{{ number_format($totalGeneralFinal, null, ',', '.') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Condiciones de venta y comportamiento. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Condiciones de venta y comportamiento (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-striped table-bordered">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Grau Ltda</th>
                                    <th>Grau Spa</th>
                                    <th>Microbox</th>
                                    <th>Tendencia Ltda</th>
                                    <th>Tendencia Spa</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <th scope="row">Total de crédito</th>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'GRAULTDA')
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ number_format($condicion->MtoCre, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'GRAUSPA')
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ number_format($condicion->MtoCre, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'MICROBOX')
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ number_format($condicion->MtoCre, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'PUBLIGRAFIKA')
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ number_format($condicion->MtoCre, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'TENSPA')
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ number_format($condicion->MtoCre, null, ',', '.') }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Condición de venta</th>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'GRAULTDA' && !is_null($condicion->CveDes))
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ $condicion->CveDes }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'GRAUSPA' && !is_null($condicion->CveDes))
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ $condicion->CveDes }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'MICROBOX' && !is_null($condicion->CveDes))
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ $condicion->CveDes }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'PUBLIGRAFIKA' && !is_null($condicion->CveDes))
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ $condicion->CveDes }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $valorExiste = false ?>
                                        @foreach ($condicionesVenta AS $condicion)
                                            @if($condicion->empresa == 'TENSPA' && !is_null($condicion->CveDes))
                                                <?php
                                                $valorExiste = true;
                                                ?>
                                                {{ $condicion->CveDes }}
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <th scope="row">Promedio de pago</th>
                                    <td>
                                        <?php $con = 0; ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'GRAULTDA')
                                                <?php $con = $con + 1; ?>
                                            @endif
                                        @endforeach
                                        <?php
                                        $sumTotal = 0;
                                        $valorExiste = false;
                                        $pasos = 0;
                                        ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'GRAULTDA')
                                                <?php
                                                $sumTotal = $promedio->DiasPago + $sumTotal;
                                                $valorExiste = true;
                                                $pasos = $pasos + 1;
                                                ?>
                                                @if ($pasos == $con)
                                                    <?php $avg = $sumTotal / $con ?>
                                                    {{ number_format($avg, null, ',', '.') }} DIAS
                                                @endif
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $con = 0; ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'GRAUSPA')
                                                <?php $con = $con + 1; ?>
                                            @endif
                                        @endforeach
                                        <?php
                                        $sumTotal = 0;
                                        $valorExiste = false;
                                        $pasos = 0;
                                        ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'GRAUSPA')
                                                <?php
                                                $sumTotal = $promedio->DiasPago + $sumTotal;
                                                $valorExiste = true;
                                                $pasos = $pasos + 1;
                                                ?>
                                                @if ($pasos == $con)
                                                    <?php $avg = $sumTotal / $con ?>
                                                    {{ number_format($avg, null, ',', '.') }} DIAS
                                                @endif
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $con = 0; ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'MICROBOX')
                                                <?php $con = $con + 1; ?>
                                            @endif
                                        @endforeach
                                        <?php
                                        $sumTotal = 0;
                                        $valorExiste = false;
                                        $pasos = 0;
                                        ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'MICROBOX')
                                                <?php
                                                $sumTotal = $promedio->DiasPago + $sumTotal;
                                                $valorExiste = true;
                                                $pasos = $pasos + 1;
                                                ?>
                                                @if ($pasos == $con)
                                                    <?php $avg = $sumTotal / $con ?>
                                                    {{ number_format($avg, null, ',', '.') }} DIAS
                                                @endif
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $con = 0; ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'PUBLIGRAFIKA')
                                                <?php $con = $con + 1; ?>
                                            @endif
                                        @endforeach
                                        <?php
                                        $sumTotal = 0;
                                        $valorExiste = false;
                                        $pasos = 0;
                                        ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'PUBLIGRAFIKA')
                                                <?php
                                                $sumTotal = $promedio->DiasPago + $sumTotal;
                                                $valorExiste = true;
                                                $pasos = $pasos + 1;
                                                ?>
                                                @if ($pasos == $con)
                                                    <?php $avg = $sumTotal / $con ?>
                                                    {{ number_format($avg, null, ',', '.') }} DIAS
                                                @endif
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                    <td>
                                        <?php $con = 0; ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'TENSPA')
                                                <?php $con = $con + 1; ?>
                                            @endif
                                        @endforeach
                                        <?php
                                        $sumTotal = 0;
                                        $valorExiste = false;
                                        $pasos = 0;
                                        ?>
                                        @foreach ($promedioPago AS $promedio)
                                            @if($promedio->empresa == 'TENSPA')
                                                <?php
                                                $sumTotal = $promedio->DiasPago + $sumTotal;
                                                $valorExiste = true;
                                                $pasos = $pasos + 1;
                                                ?>
                                                @if ($pasos == $con)
                                                    <?php $avg = $sumTotal / $con ?>
                                                    {{ number_format($avg, null, ',', '.') }} DIAS
                                                @endif
                                            @endif
                                        @endforeach

                                        @if(!$valorExiste)
                                            <span class="text-center-tb-so">-</span>
                                        @endif
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <?php $existeSeguimiento = false; ?>
        <!-- Detalle de facturas. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Detalle de facturas (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($facturasMasVencidasPorVencer) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>N° Factura</th>
                                        <th>F. Emisión</th>
                                        <th>F. Vencimiento</th>
                                        <th>Días para vencer</th>
                                        <th>Subtotal</th>
                                        <th>Estado</th>
                                        <th>Seguimiento</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $folios = [];
                                    $contador = 0;
                                    ?>
                                    @foreach($facturasMasVencidasPorVencer as $factura)

                                        @if (!in_array($factura->Folio, $folios) && $contador < $facturasMasVencidasPorVencerMostrar)

                                            {{-- Comprobamos si existen seguimientos. --}}
                                            @if (!is_null($factura->ClaveCob))
                                                <?php $existeSeguimiento = true; ?>
                                            @endif

                                            <?php
                                            //Formato solo fechas.
                                            $createDate = new DateTime($factura->Fecha);
                                            $factura->Fecha = $createDate->format('Y-m-d');

                                            $createDate = new DateTime($factura->FechaVenc);
                                            $factura->FechaVenc = $createDate->format('Y-m-d');

                                            $facturasVencidas = array();

                                            if (!is_null($factura->FechaUltAbono)) {
                                                $createDateAbono = new DateTime($factura->FechaUltAbono);
                                                $factura->FechaUltAbono = $createDateAbono->format('Y-m-d');
                                            }
                                            ?>
                                            <tr class="@if($factura->DiasConv - $factura->DiasTran < 0) danger @endif">
                                                <td>{{ $factura->empresa }}</td>
                                                <td>{{ $factura->Folio }}</td>
                                                <td>{{ $factura->Fecha }}</td>
                                                <td>{{ $factura->FechaVenc }}</td>
                                                <td>
                                                    {{ $factura->DiasConv - $factura->DiasTran }}
                                                </td>
                                                <td>{{ number_format($factura->SubTotal, null, ',', '.') }}</td>
                                                <td>
                                                    @if($factura->estadoFactura == "Vencido")
                                                        <?php array_push($facturasVencidas, $factura->Folio) ?>
                                                        <span class="label label-danger">Vencido</span>
                                                    @else
                                                        <span class="label label-success">Por vencer</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if (!is_null($factura->ClaveCob))
                                                        <a class="btn btn-default">Seguimiento</a>
                                                    @else
                                                        <a class="btn btn-default disabled">Sin seguimiento</a>
                                                    @endif
                                                </td>
                                            </tr>
                                            {{-- Guardamos el folio --}}
                                            <?php
                                            array_push($folios, $factura->Folio);
                                            $contador++;
                                            ?>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El cliente actual no posee facturas vencidas o por vencer.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Seguimiento de facturas. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Seguimiento de facturas (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        @if ($existeSeguimiento)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>N° Factura</th>
                                        <th>Fecha cobranza</th>
                                        <th>Conversación</th>
                                        <th>Resolución</th>
                                        <th>Cobrador</th>
                                        <th>Vendedor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($facturasMasVencidasPorVencer as $factura)
                                        @if (in_array($factura->Folio, $folios) && (!is_null($factura->ClaveCob)))
                                            <tr @if(in_array($factura->Folio, $facturasVencidas)) class="danger" @endif>
                                                <td>{{ $factura->empresa }}</td>
                                                <td>{{ $factura->Folio }}</td>
                                                <?php
                                                $createDateFecPComp = new DateTime($factura->FecPComp);
                                                $factura->FecPComp = $createDateFecPComp->format('Y-m-d');
                                                ?>
                                                <td>{{ $factura->FecPComp }}</td>
                                                <td>{{ $factura->conversa }}</td>
                                                <td>{{ $factura->descomp }}</td>
                                                <td>{{ $factura->CobDes }}</td>
                                                <td>{{ $factura->VenDes }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El cliente actual no posee seguimiento para las facturas vencidas o por vencer anteriormente listadas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <hr>

        <!-- Inventario de productos. -->
        <div class="row">
            <div class="col-sm-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Inventario de productos</h3>
                    </div>
                    <div class="panel-body">
                        @if (count($productosDeCliente) > 0)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Empresa</th>
                                        <th>Cod. Producto</th>
                                        <th>Desc. Producto</th>
                                        <th>U. Medida</th>
                                        <th>Precio Lista</th>
                                        <th>Stock</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($productosDeCliente AS $producto)
                                        <tr>
                                            <td>{{ $producto->empresa }}</td>
                                            <td>{{ $producto->CodProd }}</td>
                                            <td>{{ $producto->DesProd }}</td>
                                            <td>{{ $producto->CodUMed }}</td>
                                            <td>{{ number_format($producto->CostoUnitario, null, ',', '.') }}</td>
                                            <td>{{ number_format($producto->Stock, null, ',', '.') }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;El cliente actual no posee inventario en ninguna de las empresas.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection