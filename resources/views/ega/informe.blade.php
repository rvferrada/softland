@extends('layout.app')

@section('title')
    Informe EGA
@endsection

@section('body')
    <h3 class="title-so">
        <i class="fa fa-wpforms" aria-hidden="true"></i>
        &nbsp;Informe EGA
    </h3>
    <hr>

    <div class="wrapper">
        <div class="row">
            <div class="col-sm-12">
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <th scope="row">Fecha de informe</th>
                            <td>{{ $fecha }}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <hr>

        <!-- Información de valores actuales. -->
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Información de valores actuales (Sin IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>{{ $ano }} {{ $meses[$mes-1] }}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                        <?php $totalEmpresas = 0 ?>
                                        @foreach ($totalActualEmpresas as $totalObj)
                                            <tr>
                                                <th scope="row">{{ $totalObj->empresa }}</th>
                                                <?php $totalEmpresas = $totalObj->total + $totalEmpresas ?>
                                                <td>{{ number_format($totalObj->total, null, ',', '.') }}</td>

                                                @if ($totalObj->empresa == "GRAU LTDA")
                                                    <?php $totalAnoActualGrauLtda = $totalObj->total ?>
                                                @elseif ($totalObj->empresa == "GRAU SPA")
                                                    <?php $totalAnoActualGrauSpa = $totalObj->total ?>
                                                @elseif ($totalObj->empresa == "MICROBOX")
                                                    <?php $totalAnoActualMicrobox = $totalObj->total ?>
                                                @elseif ($totalObj->empresa == "TENDENCIA LTDA")
                                                    <?php $totalAnoActualTenLtda = $totalObj->total ?>
                                                @elseif ($totalObj->empresa == "TENDENCIA SPA")
                                                    <?php $totalAnoActualTenSpa = $totalObj->total ?>
                                                @endif
                                            </tr>
                                        @endforeach
                                    <tr>
                                        <th scope="row">Total Neto</th>
                                        <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Facturas por cobrar históricas (Con IVA.)</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-sm table-hover table-striped table-bordered">
                                <tbody>
                                <?php $totalEmpresas = 0 ?>
                                @foreach ($facturasPorCobrar as $totalObj)
                                    <tr>
                                        <th scope="row">{{ $totalObj->empresa }}</th>
                                        <?php $totalEmpresas = $totalObj->total + $totalEmpresas ?>
                                        <td>{{ number_format($totalObj->total, null, ',', '.') }}</td>

                                        @if ($totalObj->empresa == "GRAU LTDA")
                                            <?php $totalAnoActualGrauLtda = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "GRAU SPA")
                                            <?php $totalAnoActualGrauSpa = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "MICROBOX")
                                            <?php $totalAnoActualMicrobox = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "TENDENCIA LTDA")
                                            <?php $totalAnoActualTenLtda = $totalObj->total ?>
                                        @elseif ($totalObj->empresa == "TENDENCIA SPA")
                                            <?php $totalAnoActualTenSpa = $totalObj->total ?>
                                        @endif
                                    </tr>
                                @endforeach
                                <tr>
                                    <th scope="row">Total</th>
                                    <td>{{ number_format($totalEmpresas, null, ',', '.') }}</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <h3 class="title-so">
            <i class="fa fa-wpforms" aria-hidden="true"></i>
            &nbsp;Factoring por empresa
        </h3>

        <hr>
        <?php
            $factGR = false;
            $factGS = false;
            $factMB = false;
            $factTL = false;
            $factTS = false;
        ?>

        @foreach($totalFactoring as $factoring)
            @if($factoring->Empresa == 'GRAULTDA')
                <?php $factGR = true ?>
            @endif
            @if($factoring->Empresa == 'GRAUSPA')
                <?php $factGS = true ?>
            @endif
            @if($factoring->Empresa == 'MICROBOX')
                <?php $factMB = true ?>
            @endif
            @if($factoring->Empresa == 'PUBLIGRAFIKA')
                <?php $factTL = true ?>
            @endif
            @if($factoring->Empresa == 'TENSPA')
                <?php $factTS = true ?>
            @endif
        @endforeach

        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Factoring <strong>GRAU LTDA</strong></h3>
                    </div>
                    <div class="panel-body">
                        @if ($factGR)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Código</th>
                                            <th>Factoring</th>
                                            <th>Valor</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php $totalFact = 0 ?>
                                        @foreach($totalFactoring as $factoring)
                                            @if($factoring->Empresa == 'GRAULTDA')
                                                <?php $totalFact = $totalFact +  $factoring->DIFF ?>
                                                <tr>
                                                    <td>{{ $factoring->PCCODI }}</td>
                                                    <td>{{ $factoring->PCDESC }}</td>
                                                    <td>{{ number_format($factoring->DIFF, null, ',', '.') }}</td>
                                                </tr>
                                            @endif
                                        @endforeach
                                        <tr>
                                            <td><span class="text-center-tb-so">-</span></td>
                                            <td><span class="text-center-tb-so">-</span></td>
                                            <td><span class="text-center-tb-so">-</span></td>
                                        </tr>
                                        <tr>
                                            <td><span class="text-center-tb-so">-</span></td>
                                            <th scope="row">Total</th>
                                            <td>{{ number_format($totalFact, null, ',', '.') }}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;La empresa <strong>GRAU LTDA</strong> no posee factoring.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Factoring <strong>GRAU SPA</strong></h3>
                    </div>
                    <div class="panel-body">
                        @if ($factGS)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Factoring</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $totalFact = 0 ?>
                                    @foreach($totalFactoring as $factoring)
                                        @if($factoring->Empresa == 'GRAUSPA')
                                            <?php $totalFact = $totalFact +  $factoring->DIFF ?>
                                            <tr>
                                                <td>{{ $factoring->PCCODI }}</td>
                                                <td>{{ $factoring->PCDESC }}</td>
                                                <td>{{ number_format($factoring->DIFF, null, ',', '.') }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <th scope="row">Total</th>
                                        <td>{{ number_format($totalFact, null, ',', '.') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;La empresa <strong>GRAU SPA</strong> no posee factoring.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- MICROBOX - PUBLIGRAFIKA -->
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Factoring <strong>MICROBOX</strong></h3>
                    </div>
                    <div class="panel-body">
                        @if ($factMB)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Factoring</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $totalFact = 0 ?>
                                    @foreach($totalFactoring as $factoring)
                                        @if($factoring->Empresa == 'MICROBOX')
                                            <?php $totalFact = $totalFact +  $factoring->DIFF ?>
                                            <tr>
                                                <td>{{ $factoring->PCCODI }}</td>
                                                <td>{{ $factoring->PCDESC }}</td>
                                                <td>{{ number_format($factoring->DIFF, null, ',', '.') }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <th scope="row">Total</th>
                                        <td>{{ number_format($totalFact, null, ',', '.') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;La empresa <strong>MICROBOX</strong> no posee factoring.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Factoring <strong>TENDENCIA LTDA</strong></h3>
                    </div>
                    <div class="panel-body">
                        @if ($factTL)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Factoring</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $totalFact = 0 ?>
                                    @foreach($totalFactoring as $factoring)
                                        @if($factoring->Empresa == 'PUBLIGRAFIKA')
                                            <?php $totalFact = $totalFact +  $factoring->DIFF ?>
                                            <tr>
                                                <td>{{ $factoring->PCCODI }}</td>
                                                <td>{{ $factoring->PCDESC }}</td>
                                                <td>{{ number_format($factoring->DIFF, null, ',', '.') }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <th scope="row">Total</th>
                                        <td>{{ number_format($totalFact, null, ',', '.') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;La empresa <strong>TENDENCIA LTDA</strong> no posee factoring.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>

        <!-- TENSPA -->
        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">Factoring <strong>TENDENCIA SPA</strong></h3>
                    </div>
                    <div class="panel-body">
                        @if ($factTS)
                            <div class="table-responsive">
                                <table class="table table-sm table-hover table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Código</th>
                                        <th>Factoring</th>
                                        <th>Valor</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $totalFact = 0 ?>
                                    @foreach($totalFactoring as $factoring)
                                        @if($factoring->Empresa == 'TENSPA')
                                            <?php $totalFact = $totalFact +  $factoring->DIFF ?>
                                            <tr>
                                                <td>{{ $factoring->PCCODI }}</td>
                                                <td>{{ $factoring->PCDESC }}</td>
                                                <td>{{ number_format($factoring->DIFF, null, ',', '.') }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <td><span class="text-center-tb-so">-</span></td>
                                    </tr>
                                    <tr>
                                        <td><span class="text-center-tb-so">-</span></td>
                                        <th scope="row">Total</th>
                                        <td>{{ number_format($totalFact, null, ',', '.') }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        @else
                            {{-- Info --}}
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="alert alert-info">
                                        <i class="fa fa-info-circle" aria-hidden="true"></i>
                                        &nbsp;La empresa <strong>TENDENCIA SPA</strong> no posee factoring.
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection