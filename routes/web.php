<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* Clientes */
Route::get('/', 'ClienteController@buscador');
Route::get('c', 'ClienteController@index');

/* Ajax buscador */
Route::get('b/rut', 'BuscadorController@rut');
Route::get('b/nombre', 'BuscadorController@nombre');

/* Vendedores */
Route::get('vendedores', 'VendedorController@buscador');
Route::get('v', 'VendedorController@index');

/* EGA */
Route::get('ega', 'EgaController@index');

/* Comercial */
Route::get('comercial', 'ComercialController@index');