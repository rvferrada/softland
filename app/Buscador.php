<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buscador extends Model
{
    public static function getNombresGrauLtda($nombre, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' NomAux AS value, CodAux AS data
            FROM
                GRAULTDA.softland.cwtauxi
            WHERE
                NomAux LIKE \'%' . $nombre . '%\'
            ORDER BY
                NomAux ASC
        '));

        return $sql;
    }

    public static function getNombresGrauSpa($nombre, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' NomAux AS value, CodAux AS data
            FROM
                GRAUSPA.softland.cwtauxi
            WHERE
                NomAux LIKE \'%' . $nombre . '%\'
            ORDER BY
                NomAux ASC
        '));

        return $sql;
    }

    public static function getNombresMicrobox($nombre, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' NomAux AS value, CodAux AS data
            FROM
                MICROBOX.softland.cwtauxi
            WHERE
                NomAux LIKE \'%' . $nombre . '%\'
            ORDER BY
                NomAux ASC
        '));

        return $sql;
    }

    public static function getNombresTendenciaLtda($nombre, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' NomAux AS value, CodAux AS data
            FROM
                PUBLIGRAFIKA.softland.cwtauxi
            WHERE
                NomAux LIKE \'%' . $nombre . '%\'
            ORDER BY
                NomAux ASC
        '));

        return $sql;
    }

    public static function getNombresTendenciaSpa($nombre, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' NomAux AS value, CodAux AS data
            FROM
                TENSPA.softland.cwtauxi
            WHERE
                NomAux LIKE \'%' . $nombre . '%\'
            ORDER BY
                NomAux ASC
        '));

        return $sql;
    }

    public static function getRutGrauLtda($rut, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' CodAux AS value, NomAux AS data
            FROM
                GRAULTDA.softland.cwtauxi
            WHERE
                CodAux LIKE \'%' . $rut . '%\'
            ORDER BY
                CodAux ASC
        '));

        return $sql;
    }

    public static function getRutGrauSpa($rut, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' CodAux AS value, NomAux AS data
            FROM
                GRAUSPA.softland.cwtauxi
            WHERE
                CodAux LIKE \'%' . $rut . '%\'
            ORDER BY
                CodAux ASC
        '));

        return $sql;
    }

    public static function getRutMicrobox($rut, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' CodAux AS value, NomAux AS data
            FROM
                MICROBOX.softland.cwtauxi
            WHERE
                CodAux LIKE \'%' . $rut . '%\'
            ORDER BY
                CodAux ASC
        '));

        return $sql;
    }

    public static function getRutTendenciaLtda($rut, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' CodAux AS value, NomAux AS data
            FROM
                PUBLIGRAFIKA.softland.cwtauxi
            WHERE
                CodAux LIKE \'%' . $rut . '%\'
            ORDER BY
                CodAux ASC
        '));

        return $sql;
    }

    public static function getRutTendenciaSpa($rut, $top) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP ' . $top . ' CodAux AS value, NomAux AS data
            FROM
                TENSPA.softland.cwtauxi
            WHERE
                CodAux LIKE \'%' . $rut . '%\'
            ORDER BY
                CodAux ASC
        '));

        return $sql;
    }
}
