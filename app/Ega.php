<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ega extends Model
{
    public static function totalActualEmpresas() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (SubTotal) AS total,
                \'GRAU LTDA\' AS empresa
            FROM
                GRAULTDA.softland.iw_gsaen
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR (Fecha) = YEAR (getDate())
            AND MONTH (Fecha) = MONTH (getDate()) 
            
            UNION ALL
                SELECT
                    SUM (SubTotal) AS total,
                    \'GRAU SPA\' AS empresa
                FROM
                    GRAUSPA.softland.iw_gsaen
                WHERE
                    (Tipo = \'F\' OR Tipo = \'N\')
                AND Estado = \'V\'
                AND YEAR (Fecha) = YEAR (getDate())
                AND MONTH (Fecha) = MONTH (getDate())
                
                UNION ALL
                    SELECT
                        SUM (SubTotal) AS total,
                        \'MICROBOX\' AS empresa
                    FROM
                        MICROBOX.softland.iw_gsaen
                    WHERE
                        (Tipo = \'F\' OR Tipo = \'N\')
                    AND Estado = \'V\'
                    AND YEAR (Fecha) = YEAR (getDate())
                    AND MONTH (Fecha) = MONTH (getDate())
                    
                    UNION ALL
                        SELECT
                            SUM (SubTotal) AS total,
                            \'TENDENCIA LTDA\' AS empresa
                        FROM
                            PUBLIGRAFIKA.softland.iw_gsaen
                        WHERE
                            (Tipo = \'F\' OR Tipo = \'N\')
                        AND Estado = \'V\'
                        AND YEAR (Fecha) = YEAR (getDate())
                        AND MONTH (Fecha) = MONTH (getDate())
                        
                        UNION ALL
                            SELECT
                                SUM (SubTotal) AS total,
                                \'TENDENCIA SPA\' AS empresa
                            FROM
                                TENSPA.softland.iw_gsaen
                            WHERE
                                (Tipo = \'F\' OR Tipo = \'N\')
                            AND Estado = \'V\'
                            AND YEAR (Fecha) = YEAR (getDate())
                            AND MONTH (Fecha) = MONTH (getDate())
                            
                            ORDER BY
                                Empresa ASC        
        '));

        //La instrucción ORDER BY determina el orden de visualización en la tabla.
        return $sql;
    }

    public static function totalVencidoYporVencerEmpresas() {

        $sql = \DB::select(\DB::raw('
            SELECT SUM(Total) AS total, empresa
            FROM
            
            (SELECT
                CASE
                WHEN GR_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'GRAU LTDA\' AS empresa,
                GR_iw_gsaen.Folio,
                GR_cwmovim_hd.DEBE,
                GR_cwmovim_hd.HABER,
                GR_iw_gsaen.SubTotal,
                GR_iw_gsaen.FechaVenc,
                GR_iw_gsaen.Total
            
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    GRAULTDA.softland.cwmovim AS GR_cwmovim
                WHERE
                    GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    GR_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY empresa /*GROUP BY estadoFactura, empresa*/
             
            UNION ALL
            
            SELECT SUM(Total) AS total, empresa
            FROM
            
            (SELECT
                CASE
                WHEN GS_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'GRAU SPA\' AS empresa,
                GS_iw_gsaen.Folio,
                GS_cwmovim_hd.DEBE,
                GS_cwmovim_hd.HABER,
                GS_iw_gsaen.SubTotal,
                GS_iw_gsaen.FechaVenc,
                GS_iw_gsaen.Total
            
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    GRAUSPA.softland.cwmovim AS GS_cwmovim
                WHERE
                    GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    GS_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY empresa
            
            UNION ALL 
            
            SELECT SUM(Total) AS total, empresa
            FROM
            
            (SELECT
                CASE
                WHEN MB_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'MICROBOX\' AS empresa,
                MB_iw_gsaen.Folio,
                MB_cwmovim_hd.DEBE,
                MB_cwmovim_hd.HABER,
                MB_iw_gsaen.SubTotal,
                MB_iw_gsaen.FechaVenc,
                MB_iw_gsaen.Total
            
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    MICROBOX.softland.cwmovim AS MB_cwmovim
                WHERE
                    MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    MB_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY empresa
                     
            UNION ALL 
                                
            SELECT SUM(Total) AS total, empresa
            FROM
            
            (SELECT
                CASE
                WHEN TL_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'TENDENCIA LTDA\' AS empresa,
                TL_iw_gsaen.Folio,
                TL_cwmovim_hd.DEBE,
                TL_cwmovim_hd.HABER,
                TL_iw_gsaen.SubTotal,
                TL_iw_gsaen.FechaVenc,
                TL_iw_gsaen.Total
            
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                WHERE
                    TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    TL_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY empresa                               
                        
            UNION ALL 
                                   
            SELECT SUM(Total) AS total, empresa
            FROM
            
            (SELECT
                CASE
                WHEN TS_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'TENDENCIA SPA\' AS empresa,
                TS_iw_gsaen.Folio,
                TS_cwmovim_hd.DEBE,
                TS_cwmovim_hd.HABER,
                TS_iw_gsaen.SubTotal,
                TS_iw_gsaen.FechaVenc,
                TS_iw_gsaen.Total
            
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    TENSPA.softland.cwmovim AS TS_cwmovim
                WHERE
                    TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    TS_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY empresa                                
                        
            ORDER BY Empresa ASC        
        '));

        return $sql;
    }

    public static function totalOperacionesFactoring() {

        $sql = \DB::select(\DB::raw('
            SELECT
                \'GRAULTDA\' AS Empresa,
                FOLIOS.PCCODI,
                FOLIOS.PCDESC,
                SUM (FOLIOS.DEBE) AS DEBE,
                SUM (FOLIOS.HABER) AS HABER,
                SUM (FOLIOS.DEBE) - SUM (FOLIOS.HABER) AS DIFF
            FROM
            (SELECT
                SUM (CWMOVIM.MovDebe) AS DEBE,
                SUM (CWMOVIM.MovHaber) AS HABER,
                SUM (CWMOVIM.MovDebe) - SUM (CWMOVIM.MovHaber) AS DIFF,
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef,
                COUNT (*) AS REPT
            FROM
                GRAULTDA.softland.cwpctas AS CWPCTAS /* Unimos con la tabla que contiene los valores de cada factoring. */
            INNER JOIN GRAULTDA.softland.cwmovim AS CWMOVIM ON CWPCTAS.PCCODI = CWMOVIM.PctCod /* Condiciones globales. */
            WHERE
                CWPCTAS.PCCODI LIKE \'2-1-01-%\'
            AND CWPCTAS.PCDESC LIKE \'Op%\'
            AND CWMOVIM.CpbNum <> 00000000
            GROUP BY
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef
            HAVING
                (COUNT(*) > 0 AND (SUM (CWMOVIM.MovHaber) <> 0 OR CWMOVIM.MovNumDocRef = \'21\'))
            ) AS FOLIOS
            
            GROUP BY FOLIOS.PCCODI, FOLIOS.PCDESC
            
            UNION ALL
            
            SELECT
                \'GRAUSPA\' AS Empresa,
                FOLIOS.PCCODI,
                FOLIOS.PCDESC,
                SUM (FOLIOS.DEBE) AS DEBE,
                SUM (FOLIOS.HABER) AS HABER,
                SUM (FOLIOS.DEBE) - SUM (FOLIOS.HABER) AS DIFF
            FROM
            (SELECT
                SUM (CWMOVIM.MovDebe) AS DEBE,
                SUM (CWMOVIM.MovHaber) AS HABER,
                SUM (CWMOVIM.MovDebe) - SUM (CWMOVIM.MovHaber) AS DIFF,
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef,
                COUNT (*) AS REPT
            FROM
                GRAUSPA.softland.cwpctas AS CWPCTAS /* Unimos con la tabla que contiene los valores de cada factoring. */
            INNER JOIN GRAUSPA.softland.cwmovim AS CWMOVIM ON CWPCTAS.PCCODI = CWMOVIM.PctCod /* Condiciones globales. */
            WHERE
                CWPCTAS.PCCODI LIKE \'2-1-01-%\'
            AND CWPCTAS.PCDESC LIKE \'Op%\'
            AND CWMOVIM.CpbNum <> 00000000
            GROUP BY
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef
            HAVING
                (COUNT(*) > 0 AND (SUM (CWMOVIM.MovHaber) <> 0 OR CWMOVIM.MovNumDocRef = \'21\'))
            ) AS FOLIOS
            
            GROUP BY FOLIOS.PCCODI, FOLIOS.PCDESC
            
            
            UNION ALL
            
            SELECT
                \'MICROBOX\' AS Empresa,
                FOLIOS.PCCODI,
                FOLIOS.PCDESC,
                SUM (FOLIOS.DEBE) AS DEBE,
                SUM (FOLIOS.HABER) AS HABER,
                SUM (FOLIOS.DEBE) - SUM (FOLIOS.HABER) AS DIFF
            FROM
            (SELECT
                SUM (CWMOVIM.MovDebe) AS DEBE,
                SUM (CWMOVIM.MovHaber) AS HABER,
                SUM (CWMOVIM.MovDebe) - SUM (CWMOVIM.MovHaber) AS DIFF,
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef,
                COUNT (*) AS REPT
            FROM
                MICROBOX.softland.cwpctas AS CWPCTAS /* Unimos con la tabla que contiene los valores de cada factoring. */
            INNER JOIN MICROBOX.softland.cwmovim AS CWMOVIM ON CWPCTAS.PCCODI = CWMOVIM.PctCod /* Condiciones globales. */
            WHERE
                CWPCTAS.PCCODI LIKE \'2-1-01-%\'
            AND CWPCTAS.PCDESC LIKE \'Op%\'
            AND CWMOVIM.CpbNum <> 00000000
            GROUP BY
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef
            HAVING
                (COUNT(*) > 0 AND (SUM (CWMOVIM.MovHaber) <> 0 OR CWMOVIM.MovNumDocRef = \'21\'))
            ) AS FOLIOS
            
            GROUP BY FOLIOS.PCCODI, FOLIOS.PCDESC
            
            UNION ALL
            
            SELECT
                \'PUBLIGRAFIKA\' AS Empresa,
                FOLIOS.PCCODI,
                FOLIOS.PCDESC,
                SUM (FOLIOS.DEBE) AS DEBE,
                SUM (FOLIOS.HABER) AS HABER,
                SUM (FOLIOS.DEBE) - SUM (FOLIOS.HABER) AS DIFF
            FROM
            (SELECT
                SUM (CWMOVIM.MovDebe) AS DEBE,
                SUM (CWMOVIM.MovHaber) AS HABER,
                SUM (CWMOVIM.MovDebe) - SUM (CWMOVIM.MovHaber) AS DIFF,
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef,
                COUNT (*) AS REPT
            FROM
                PUBLIGRAFIKA.softland.cwpctas AS CWPCTAS /* Unimos con la tabla que contiene los valores de cada factoring. */
            INNER JOIN PUBLIGRAFIKA.softland.cwmovim AS CWMOVIM ON CWPCTAS.PCCODI = CWMOVIM.PctCod /* Condiciones globales. */
            WHERE
                CWPCTAS.PCCODI LIKE \'2-1-01-%\'
            AND CWPCTAS.PCDESC LIKE \'Op%\'
            AND CWMOVIM.CpbNum <> 00000000
            GROUP BY
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef
            HAVING
                (COUNT(*) > 0 AND (SUM (CWMOVIM.MovHaber) <> 0 OR CWMOVIM.MovNumDocRef = \'21\'))
            ) AS FOLIOS
            
            GROUP BY FOLIOS.PCCODI, FOLIOS.PCDESC
            
            UNION ALL
            
            SELECT
                \'TENSPA\' AS Empresa,
                FOLIOS.PCCODI,
                FOLIOS.PCDESC,
                SUM (FOLIOS.DEBE) AS DEBE,
                SUM (FOLIOS.HABER) AS HABER,
                SUM (FOLIOS.DEBE) - SUM (FOLIOS.HABER) AS DIFF
            FROM
            (SELECT
                SUM (CWMOVIM.MovDebe) AS DEBE,
                SUM (CWMOVIM.MovHaber) AS HABER,
                SUM (CWMOVIM.MovDebe) - SUM (CWMOVIM.MovHaber) AS DIFF,
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef,
                COUNT (*) AS REPT
            FROM
                TENSPA.softland.cwpctas AS CWPCTAS /* Unimos con la tabla que contiene los valores de cada factoring. */
            INNER JOIN TENSPA.softland.cwmovim AS CWMOVIM ON CWPCTAS.PCCODI = CWMOVIM.PctCod /* Condiciones globales. */
            WHERE
                CWPCTAS.PCCODI LIKE \'2-1-01-%\'
            AND CWPCTAS.PCDESC LIKE \'Op%\'
            AND CWMOVIM.CpbNum <> 00000000
            GROUP BY
                CWPCTAS.PCCODI,
                CWPCTAS.PCDESC,
                CWMOVIM.MovNumDocRef
            HAVING
                (COUNT(*) > 0 AND (SUM (CWMOVIM.MovHaber) <> 0 OR CWMOVIM.MovNumDocRef = \'21\'))
            ) AS FOLIOS
            
            GROUP BY FOLIOS.PCCODI, FOLIOS.PCDESC
            
            ORDER BY Empresa, FOLIOS.PCCODI        
        '));

        return $sql;
    }
}
