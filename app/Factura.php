<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{
    public static function totalAnoActual($rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
              SUM(SubTotal) AS total,
              \'GRAULTDA\' AS empresa
            FROM
              GRAULTDA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo =  \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate())
            
            UNION ALL
            
            SELECT
              SUM(SubTotal) AS total,
              \'GRAUSPA\' AS empresa
            FROM
              GRAUSPA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate())
                        
            UNION ALL
                                    
            SELECT
              SUM(SubTotal) AS total,
              \'MICROBOX\' AS empresa
            FROM
              MICROBOX.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate())   
                                               
            UNION ALL
                                                           
            SELECT
              SUM(SubTotal) AS total,
              \'PUBLIGRAFIKA\' AS empresa
            FROM
              PUBLIGRAFIKA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate())      
                                                                   
            UNION ALL
                                                           
            SELECT
              SUM(SubTotal) AS total,
              \'TENSPA\' AS empresa
            FROM
              TENSPA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate())
                                                                                                                                                                        
            ORDER BY Empresa ASC                                                                                                                                                            
        '));

        //La instrucción ORDER BY determina el orden de visualización en la tabla.
        return $sql;

    }

    public static function totalAnoAnterior($rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
              SUM(SubTotal) AS total,
              \'GRAULTDA\' AS empresa
            FROM
              GRAULTDA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate()) - 1
            
            UNION ALL
            
            SELECT
              SUM(SubTotal) AS total,
              \'GRAUSPA\' AS empresa
            FROM
              GRAUSPA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate()) - 1
                        
            UNION ALL
                                    
            SELECT
              SUM(SubTotal) AS total,
              \'MICROBOX\' AS empresa
            FROM
              MICROBOX.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate()) - 1   
                                               
            UNION ALL
                                                           
            SELECT
              SUM(SubTotal) AS total,
              \'PUBLIGRAFIKA\' AS empresa
            FROM
              PUBLIGRAFIKA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate()) - 1      
                                                                   
            UNION ALL
                                                           
            SELECT
              SUM(SubTotal) AS total,
              \'TENSPA\' AS empresa
            FROM
              TENSPA.softland.iw_gsaen
            WHERE
              CodAux = \'' . $rut . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND YEAR(Fecha) = YEAR(getDate()) - 1
                                                                                                                                                                        
            ORDER BY Empresa ASC                                                                                                                                                            
        '));

        //La instrucción ORDER BY determina el orden de visualización en la tabla.
        return $sql;

    }

    public static function totalVencidoYporVencer($rut) {

        $sql = \DB::select(\DB::raw('
            SELECT SUM(Total) AS total, estadoFactura, empresa
            FROM
            
            (SELECT
                CASE
                WHEN GR_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'GRAULTDA\' AS empresa,
                GR_iw_gsaen.Folio,
                GR_cwmovim_hd.DEBE,
                GR_cwmovim_hd.HABER,
                GR_iw_gsaen.SubTotal,
                GR_iw_gsaen.FechaVenc,
                GR_iw_gsaen.Total
            
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    GRAULTDA.softland.cwmovim AS GR_cwmovim
                WHERE
                    GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    GR_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND CodAux = \'' . $rut . '\'
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa
            
            UNION ALL
            
            SELECT SUM(Total) AS total, estadoFactura, empresa
            FROM
            
            (SELECT
                CASE
                WHEN GS_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'GRAUSPA\' AS empresa,
                GS_iw_gsaen.Folio,
                GS_cwmovim_hd.DEBE,
                GS_cwmovim_hd.HABER,
                GS_iw_gsaen.SubTotal,
                GS_iw_gsaen.FechaVenc,
                GS_iw_gsaen.Total
            
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    GRAUSPA.softland.cwmovim AS GS_cwmovim
                WHERE
                    GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    GS_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND CodAux = \'' . $rut . '\'
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa
            
            UNION ALL 
            
            SELECT SUM(Total) AS total, estadoFactura, empresa
            FROM
            
            (SELECT
                CASE
                WHEN MB_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'MICROBOX\' AS empresa,
                MB_iw_gsaen.Folio,
                MB_cwmovim_hd.DEBE,
                MB_cwmovim_hd.HABER,
                MB_iw_gsaen.SubTotal,
                MB_iw_gsaen.FechaVenc,
                MB_iw_gsaen.Total
            
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    MICROBOX.softland.cwmovim AS MB_cwmovim
                WHERE
                    MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    MB_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND CodAux = \'' . $rut . '\'
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa
                     
            UNION ALL 
                                
            SELECT SUM(Total) AS total, estadoFactura, empresa
            FROM
            
            (SELECT
                CASE
                WHEN TL_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'PUBLIGRAFIKA\' AS empresa,
                TL_iw_gsaen.Folio,
                TL_cwmovim_hd.DEBE,
                TL_cwmovim_hd.HABER,
                TL_iw_gsaen.SubTotal,
                TL_iw_gsaen.FechaVenc,
                TL_iw_gsaen.Total
            
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                WHERE
                    TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    TL_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND CodAux = \'' . $rut . '\'
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa                               
                        
            UNION ALL 
                                   
            SELECT SUM(Total) AS total, estadoFactura, empresa
            FROM
            
            (SELECT
                CASE
                WHEN TS_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                \'TENSPA\' AS empresa,
                TS_iw_gsaen.Folio,
                TS_cwmovim_hd.DEBE,
                TS_cwmovim_hd.HABER,
                TS_iw_gsaen.SubTotal,
                TS_iw_gsaen.FechaVenc,
                TS_iw_gsaen.Total
            
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            
            INNER JOIN (
                SELECT
                    SUM(MovHaber) AS HABER,
                    SUM(MovDebe) AS DEBE,
                    MovNumDocRef
                FROM
                    TENSPA.softland.cwmovim AS TS_cwmovim
                WHERE
                    TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\' AND
                    TS_cwmovim.MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            
            ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND CodAux = \'' . $rut . '\'
            AND Estado = \'V\'
            AND HABER < Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa                                
                        
            ORDER BY Empresa ASC        
        '));

        return $sql;
    }

    public static function facturasEnFactoring($rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
                ABS(SUM(MovDebe) - SUM(MovHaber)) AS total,
                \'GRAULTDA\' AS empresa
            FROM
                GRAULTDA.softland.cwmovim
            WHERE
                CodAux = \'' . $rut . '\'
            AND MovTipDocRef = \'OF\'
            AND MovGlosa <> \'Movimiento de Apertura\' 
                   
            UNION ALL
            
            SELECT
                ABS(SUM(MovDebe) - SUM(MovHaber)) AS total,
                \'GRAUSPA\' AS empresa
            FROM
                GRAUSPA.softland.cwmovim
            WHERE
                CodAux = \'' . $rut . '\'
            AND MovTipDocRef = \'OF\'
            AND MovGlosa <> \'Movimiento de Apertura\'
            
            UNION ALL 
            
            SELECT
                ABS(SUM(MovDebe) - SUM(MovHaber)) AS total,
                \'MICROBOX\' AS empresa
            FROM
                MICROBOX.softland.cwmovim
            WHERE
                CodAux = \'' . $rut . '\'
            AND MovTipDocRef = \'OF\'
            AND MovGlosa <> \'Movimiento de Apertura\'     
                   
            UNION ALL 
                  
            SELECT
                ABS(SUM(MovDebe) - SUM(MovHaber)) AS total,
                \'PUBLIGRAFIKA\' AS empresa
            FROM
                PUBLIGRAFIKA.softland.cwmovim
            WHERE
                CodAux = \'' . $rut . '\'
            AND MovTipDocRef = \'OF\'
            AND MovGlosa <> \'Movimiento de Apertura\'   
                            
            UNION ALL 
                                     
            SELECT
                ABS(SUM(MovDebe) - SUM(MovHaber)) AS total,
                \'TENSPA\' AS empresa
            FROM
                TENSPA.softland.cwmovim
            WHERE
                CodAux = \'' . $rut . '\'
            AND MovTipDocRef = \'OF\'
            AND MovGlosa <> \'Movimiento de Apertura\'   
                                              
            ORDER BY empresa ASC                                              
        '));

        return $sql;
    }

    public static function facturasMasVencidasOporVencer($rut, $cant) {

        $sql = \DB::select(\DB::raw('
            SELECT
                \'GRAULTDA\' AS empresa,
                GR_iw_gsaen.Folio,
                GR_iw_gsaen.Fecha,
                GR_iw_gsaen.FechaVenc,
                DATEDIFF(day, GR_iw_gsaen.Fecha, GETDATE()) AS DiasTran,
                DATEDIFF(day, GR_iw_gsaen.Fecha, GR_iw_gsaen.FechaVenc) AS DiasConv,
                GR_iw_gsaen.Glosa,
                GR_iw_gsaen.SubTotal,
                GR_iw_gsaen.Total,
                GR_cwmovim_hd.HABER AS Abonos,
                GR_cwmovim_hd.f AS FechaUltAbono,
                CASE
            WHEN GETDATE() > GR_iw_gsaen.FechaVenc THEN
                \'Vencido\'
            ELSE
                \'Por vencer\'
            END AS estadoFactura,
                GR_xwseguimiento.ClaveCob,
                GR_xwseguimiento.CodComp,
                GR_xwseguimiento.FecPComp,
                GR_xwcobranza.conversa,
                GR_xwcobranza.codcob,
                GR_cwtcobr.CobDes,
                GR_xwttcomp.descomp,
                GR_cwtvend.VenCod,
                GR_cwtvend.VenDes,
                GR_cwtaxco_join.NomCon,
                GR_cwtaxco_join.FonCon,
                GR_cwtaxco_join.Email
            
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
            
            LEFT JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    GRAULTDA.softland.cwmovim AS GR_cwmovim
                WHERE
                    GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND GR_cwmovim.MovGlosa <> \'\'
                AND GR_cwmovim.CodAux = \'' . $rut . '\'
                AND GR_cwmovim.CpbFec < GETDATE()
                AND (
                    GR_cwmovim.MovDebe <> 0
                    OR GR_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
            
            LEFT JOIN GRAULTDA.softland.xwseguimiento AS GR_xwseguimiento ON GR_iw_gsaen.Folio = GR_xwseguimiento.NumDoc
            
            LEFT JOIN GRAULTDA.softland.xwcobranza AS GR_xwcobranza ON GR_xwseguimiento.ClaveCob = GR_xwcobranza.ClaveCob
            
            LEFT JOIN GRAULTDA.softland.cwtcobr AS GR_cwtcobr ON GR_xwcobranza.codcob = GR_cwtcobr.CobCod
            
            LEFT JOIN GRAULTDA.softland.xwttcomp AS GR_xwttcomp ON GR_xwseguimiento.CodComp = GR_xwttcomp.codcomp
            
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod
            
            LEFT JOIN 
            (
                SELECT TOP 1 NomCon, FonCon, Email, CodAuc
                FROM GRAULTDA.softland.cwtaxco AS GR_cwtaxco
                WHERE GR_cwtaxco.CodAuc = \'' . $rut . '\'
            
            ) AS GR_cwtaxco_join ON GR_iw_gsaen.CodAux = GR_cwtaxco_join.CodAuc
            
            WHERE
                GR_iw_gsaen.CodAux = \'' . $rut . '\'
            AND (GR_iw_gsaen.Tipo = \'F\' OR GR_iw_gsaen.Tipo = \'N\')
            AND GR_iw_gsaen.Estado = \'V\'
            AND GR_cwmovim_hd.DEBE <> GR_cwmovim_hd.HABER
            AND GR_cwmovim_hd.HABER < GR_iw_gsaen.Total
            
            UNION ALL
            
            SELECT
                \'GRAUSPA\' AS empresa,
                GS_iw_gsaen.Folio,
                GS_iw_gsaen.Fecha,
                GS_iw_gsaen.FechaVenc,
                DATEDIFF(day, GS_iw_gsaen.Fecha, GETDATE()) AS DiasTran,
                DATEDIFF(day, GS_iw_gsaen.Fecha, GS_iw_gsaen.FechaVenc) AS DiasConv,
                GS_iw_gsaen.Glosa,
                GS_iw_gsaen.SubTotal,
                GS_iw_gsaen.Total,
                GS_cwmovim_hd.HABER AS Abonos,
                GS_cwmovim_hd.f AS FechaUltAbono,
                CASE
            WHEN GETDATE() > GS_iw_gsaen.FechaVenc THEN
                \'Vencido\'
            ELSE
                \'Por vencer\'
            END AS estadoFactura,
                GS_xwseguimiento.ClaveCob,
                GS_xwseguimiento.CodComp,
                GS_xwseguimiento.FecPComp,
                GS_xwcobranza.conversa,
                GS_xwcobranza.codcob,
                GS_cwtcobr.CobDes,
                GS_xwttcomp.descomp,
                GS_cwtvend.VenCod,
                GS_cwtvend.VenDes,
                GS_cwtaxco_join.NomCon,
                GS_cwtaxco_join.FonCon,
                GS_cwtaxco_join.Email
            
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
            
            LEFT JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    GRAUSPA.softland.cwmovim AS GS_cwmovim
                WHERE
                    GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND GS_cwmovim.MovGlosa <> \'\'
                AND GS_cwmovim.CodAux = \'' . $rut . '\'
                AND GS_cwmovim.CpbFec < GETDATE()
                AND (
                    GS_cwmovim.MovDebe <> 0
                    OR GS_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
            
            LEFT JOIN GRAUSPA.softland.xwseguimiento AS GS_xwseguimiento ON GS_iw_gsaen.Folio = GS_xwseguimiento.NumDoc
            
            LEFT JOIN GRAUSPA.softland.xwcobranza AS GS_xwcobranza ON GS_xwseguimiento.ClaveCob = GS_xwcobranza.ClaveCob
            
            LEFT JOIN GRAUSPA.softland.cwtcobr AS GS_cwtcobr ON GS_xwcobranza.codcob = GS_cwtcobr.CobCod
            
            LEFT JOIN GRAUSPA.softland.xwttcomp AS GS_xwttcomp ON GS_xwseguimiento.CodComp = GS_xwttcomp.codcomp
            
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod
            
            LEFT JOIN 
            (
                SELECT TOP 1 NomCon, FonCon, Email, CodAuc
                FROM GRAUSPA.softland.cwtaxco AS GS_cwtaxco
                WHERE GS_cwtaxco.CodAuc = \'' . $rut . '\'
            
            ) AS GS_cwtaxco_join ON GS_iw_gsaen.CodAux = GS_cwtaxco_join.CodAuc
            
            WHERE
                GS_iw_gsaen.CodAux = \'' . $rut . '\'
            AND (GS_iw_gsaen.Tipo = \'F\' OR GS_iw_gsaen.Tipo = \'N\')
            AND GS_iw_gsaen.Estado = \'V\'
            AND GS_cwmovim_hd.DEBE <> GS_cwmovim_hd.HABER
            AND GS_cwmovim_hd.HABER < GS_iw_gsaen.Total
            
            UNION ALL
            
            SELECT
                \'MICROBOX\' AS empresa,
                MB_iw_gsaen.Folio,
                MB_iw_gsaen.Fecha,
                MB_iw_gsaen.FechaVenc,
                DATEDIFF(day, MB_iw_gsaen.Fecha, GETDATE()) AS DiasTran,
                DATEDIFF(day, MB_iw_gsaen.Fecha, MB_iw_gsaen.FechaVenc) AS DiasConv,
                MB_iw_gsaen.Glosa,
                MB_iw_gsaen.SubTotal,
                MB_iw_gsaen.Total,
                MB_cwmovim_hd.HABER AS Abonos,
                MB_cwmovim_hd.f AS FechaUltAbono,
                CASE
            WHEN GETDATE() > MB_iw_gsaen.FechaVenc THEN
                \'Vencido\'
            ELSE
                \'Por vencer\'
            END AS estadoFactura,
                MB_xwseguimiento.ClaveCob,
                MB_xwseguimiento.CodComp,
                MB_xwseguimiento.FecPComp,
                MB_xwcobranza.conversa,
                MB_xwcobranza.codcob,
                MB_cwtcobr.CobDes,
                MB_xwttcomp.descomp,
                MB_cwtvend.VenCod,
                MB_cwtvend.VenDes,
                MB_cwtaxco_join.NomCon,
                MB_cwtaxco_join.FonCon,
                MB_cwtaxco_join.Email
            
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
            
            LEFT JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    MICROBOX.softland.cwmovim AS MB_cwmovim
                WHERE
                    MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND MB_cwmovim.MovGlosa <> \'\'
                AND MB_cwmovim.CodAux = \'' . $rut . '\'
                AND MB_cwmovim.CpbFec < GETDATE()
                AND (
                    MB_cwmovim.MovDebe <> 0
                    OR MB_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
            
            LEFT JOIN MICROBOX.softland.xwseguimiento AS MB_xwseguimiento ON MB_iw_gsaen.Folio = MB_xwseguimiento.NumDoc
            
            LEFT JOIN MICROBOX.softland.xwcobranza AS MB_xwcobranza ON MB_xwseguimiento.ClaveCob = MB_xwcobranza.ClaveCob
            
            LEFT JOIN MICROBOX.softland.cwtcobr AS MB_cwtcobr ON MB_xwcobranza.codcob = MB_cwtcobr.CobCod
            
            LEFT JOIN MICROBOX.softland.xwttcomp AS MB_xwttcomp ON MB_xwseguimiento.CodComp = MB_xwttcomp.codcomp
            
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod
            
            LEFT JOIN 
            (
                SELECT TOP 1 NomCon, FonCon, Email, CodAuc
                FROM MICROBOX.softland.cwtaxco AS MB_cwtaxco
                WHERE MB_cwtaxco.CodAuc = \'' . $rut . '\'
            
            ) AS MB_cwtaxco_join ON MB_iw_gsaen.CodAux = MB_cwtaxco_join.CodAuc
            
            WHERE
                MB_iw_gsaen.CodAux = \'' . $rut . '\'
            AND (MB_iw_gsaen.Tipo = \'F\' OR MB_iw_gsaen.Tipo = \'N\')
            AND MB_iw_gsaen.Estado = \'V\'
            AND MB_cwmovim_hd.DEBE <> MB_cwmovim_hd.HABER
            AND MB_cwmovim_hd.HABER < MB_iw_gsaen.Total
            
            UNION ALL
            
            SELECT
                \'PUBLIGRAFIKA\' AS empresa,
                TL_iw_gsaen.Folio,
                TL_iw_gsaen.Fecha,
                TL_iw_gsaen.FechaVenc,
                DATEDIFF(day, TL_iw_gsaen.Fecha, GETDATE()) AS DiasTran,
                DATEDIFF(day, TL_iw_gsaen.Fecha, TL_iw_gsaen.FechaVenc) AS DiasConv,
                TL_iw_gsaen.Glosa,
                TL_iw_gsaen.SubTotal,
                TL_iw_gsaen.Total,
                TL_cwmovim_hd.HABER AS Abonos,
                TL_cwmovim_hd.f AS FechaUltAbono,
                CASE
            WHEN GETDATE() > TL_iw_gsaen.FechaVenc THEN
                \'Vencido\'
            ELSE
                \'Por vencer\'
            END AS estadoFactura,
                TL_xwseguimiento.ClaveCob,
                TL_xwseguimiento.CodComp,
                TL_xwseguimiento.FecPComp,
                TL_xwcobranza.conversa,
                TL_xwcobranza.codcob,
                TL_cwtcobr.CobDes,
                TL_xwttcomp.descomp,
                TL_cwtvend.VenCod,
                TL_cwtvend.VenDes,
                TL_cwtaxco_join.NomCon,
                TL_cwtaxco_join.FonCon,
                TL_cwtaxco_join.Email
            
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            
            LEFT JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                WHERE
                    TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND TL_cwmovim.MovGlosa <> \'\'
                AND TL_cwmovim.CodAux = \'' . $rut . '\'
                AND TL_cwmovim.CpbFec < GETDATE()
                AND (
                    TL_cwmovim.MovDebe <> 0
                    OR TL_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
            
            LEFT JOIN PUBLIGRAFIKA.softland.xwseguimiento AS TL_xwseguimiento ON TL_iw_gsaen.Folio = TL_xwseguimiento.NumDoc
            
            LEFT JOIN PUBLIGRAFIKA.softland.xwcobranza AS TL_xwcobranza ON TL_xwseguimiento.ClaveCob = TL_xwcobranza.ClaveCob
            
            LEFT JOIN PUBLIGRAFIKA.softland.cwtcobr AS TL_cwtcobr ON TL_xwcobranza.codcob = TL_cwtcobr.CobCod
            
            LEFT JOIN PUBLIGRAFIKA.softland.xwttcomp AS TL_xwttcomp ON TL_xwseguimiento.CodComp = TL_xwttcomp.codcomp
            
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod
            
            LEFT JOIN 
            (
                SELECT TOP 1 NomCon, FonCon, Email, CodAuc
                FROM PUBLIGRAFIKA.softland.cwtaxco AS TL_cwtaxco
                WHERE TL_cwtaxco.CodAuc = \'' . $rut . '\'
            
            ) AS TL_cwtaxco_join ON TL_iw_gsaen.CodAux = TL_cwtaxco_join.CodAuc
            
            WHERE
                TL_iw_gsaen.CodAux = \'' . $rut . '\'
            AND (TL_iw_gsaen.Tipo = \'F\' OR TL_iw_gsaen.Tipo = \'N\')
            AND TL_iw_gsaen.Estado = \'V\'
            AND TL_cwmovim_hd.DEBE <> TL_cwmovim_hd.HABER
            AND TL_cwmovim_hd.HABER < TL_iw_gsaen.Total
            
            UNION ALL
            
            SELECT
                \'TENSPA\' AS empresa,
                TS_iw_gsaen.Folio,
                TS_iw_gsaen.Fecha,
                TS_iw_gsaen.FechaVenc,
                DATEDIFF(day, TS_iw_gsaen.Fecha, GETDATE()) AS DiasTran,
                DATEDIFF(day, TS_iw_gsaen.Fecha, TS_iw_gsaen.FechaVenc) AS DiasConv,
                TS_iw_gsaen.Glosa,
                TS_iw_gsaen.SubTotal,
                TS_iw_gsaen.Total,
                TS_cwmovim_hd.HABER AS Abonos,
                TS_cwmovim_hd.f AS FechaUltAbono,
                CASE
            WHEN GETDATE() > TS_iw_gsaen.FechaVenc THEN
                \'Vencido\'
            ELSE
                \'Por vencer\'
            END AS estadoFactura,
                TS_xwseguimiento.ClaveCob,
                TS_xwseguimiento.CodComp,
                TS_xwseguimiento.FecPComp,
                TS_xwcobranza.conversa,
                TS_xwcobranza.codcob,
                TS_cwtcobr.CobDes,
                TS_xwttcomp.descomp,
                TS_cwtvend.VenCod,
                TS_cwtvend.VenDes,
                TS_cwtaxco_join.NomCon,
                TS_cwtaxco_join.FonCon,
                TS_cwtaxco_join.Email
            
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            
            LEFT JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    TENSPA.softland.cwmovim AS TS_cwmovim
                WHERE
                    TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND TS_cwmovim.MovGlosa <> \'\'
                AND TS_cwmovim.CodAux = \'' . $rut . '\'
                AND TS_cwmovim.CpbFec < GETDATE()
                AND (
                    TS_cwmovim.MovDebe <> 0
                    OR TS_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
            
            LEFT JOIN TENSPA.softland.xwseguimiento AS TS_xwseguimiento ON TS_iw_gsaen.Folio = TS_xwseguimiento.NumDoc
            
            LEFT JOIN TENSPA.softland.xwcobranza AS TS_xwcobranza ON TS_xwseguimiento.ClaveCob = TS_xwcobranza.ClaveCob
            
            LEFT JOIN TENSPA.softland.cwtcobr AS TS_cwtcobr ON TS_xwcobranza.codcob = TS_cwtcobr.CobCod
            
            LEFT JOIN TENSPA.softland.xwttcomp AS TS_xwttcomp ON TS_xwseguimiento.CodComp = TS_xwttcomp.codcomp
            
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod
            
            LEFT JOIN 
            (
                SELECT TOP 1 NomCon, FonCon, Email, CodAuc
                FROM TENSPA.softland.cwtaxco AS TS_cwtaxco
                WHERE TS_cwtaxco.CodAuc = \'' . $rut . '\'
            
            ) AS TS_cwtaxco_join ON TS_iw_gsaen.CodAux = TS_cwtaxco_join.CodAuc
            
            WHERE
                TS_iw_gsaen.CodAux = \'' . $rut . '\'
            AND (TS_iw_gsaen.Tipo = \'F\' OR TS_iw_gsaen.Tipo = \'N\')
            AND TS_iw_gsaen.Estado = \'V\'
            AND TS_cwmovim_hd.DEBE <> TS_cwmovim_hd.HABER
            AND TS_cwmovim_hd.HABER < TS_iw_gsaen.Total
            
            ORDER BY FechaVenc DESC OFFSET 0 ROWS FETCH NEXT ' . $cant . ' ROWS ONLY        
        '));

        return $sql;
    }

    public static function promedioPago($rut, $fact) {

        $sql = \DB::select(\DB::raw('
            SELECT
                prom_pago.empresa, prom_pago.DiasPago 
            FROM
                (
                    SELECT
                        \'GRAULTDA\' AS empresa,
                        GR_iw_gsaen.Folio,
                        GR_iw_gsaen.Fecha,
                        GR_iw_gsaen.FechaVenc,
                        DATEDIFF(
                            DAY,
                            GR_iw_gsaen.Fecha,
                            GR_cwmovim_hd.f
                        ) AS DiasPago,
                        GR_cwmovim_hd.HABER AS Abonos,
                        GR_cwmovim_hd.f AS FechaUltAbono
                    FROM
                        GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
                    LEFT JOIN (
                        SELECT
                            SUM (MovDebe) AS DEBE,
                            SUM (MovHaber) AS HABER,
                            MovNumDocRef,
                            MAX (FecPag) AS f
                        FROM
                            GRAULTDA.softland.cwmovim AS GR_cwmovim
                        WHERE
                            GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                        AND GR_cwmovim.MovGlosa <> \'\'
                        AND GR_cwmovim.CodAux = \'' . $rut . '\'
                        AND GR_cwmovim.CpbFec < GETDATE()
                        AND (
                            GR_cwmovim.MovDebe <> 0
                            OR GR_cwmovim.MovHaber <> 0
                        )
                        AND MovTipDocRef = \'FV\'
                        GROUP BY
                            MovNumDocRef
                    ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
                    WHERE
                        GR_iw_gsaen.CodAux = \'' . $rut . '\'
                    AND (GR_iw_gsaen.Tipo = \'F\' OR GR_iw_gsaen.Tipo = \'N\')
                    AND GR_iw_gsaen.Estado = \'V\'
                    AND GR_cwmovim_hd.DEBE = GR_cwmovim_hd.HABER
                    AND GR_cwmovim_hd.HABER = GR_iw_gsaen.Total
                    ORDER BY
                        Fecha DESC OFFSET 0 ROWS FETCH NEXT ' . $fact . ' ROWS ONLY
                    UNION ALL
                        SELECT
                            \'GRAUSPA\' AS empresa,
                            GS_iw_gsaen.Folio,
                            GS_iw_gsaen.Fecha,
                            GS_iw_gsaen.FechaVenc,
                            DATEDIFF(
                                DAY,
                                GS_iw_gsaen.Fecha,
                                GS_cwmovim_hd.f
                            ) AS DiasPago,
                            GS_cwmovim_hd.HABER AS Abonos,
                            GS_cwmovim_hd.f AS FechaUltAbono
                        FROM
                            GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
                        LEFT JOIN (
                            SELECT
                                SUM (MovDebe) AS DEBE,
                                SUM (MovHaber) AS HABER,
                                MovNumDocRef,
                                MAX (FecPag) AS f
                            FROM
                                GRAUSPA.softland.cwmovim AS GS_cwmovim
                            WHERE
                                GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                            AND GS_cwmovim.MovGlosa <> \'\'
                            AND GS_cwmovim.CodAux = \'' . $rut . '\'
                            AND GS_cwmovim.CpbFec < GETDATE()
                            AND (
                                GS_cwmovim.MovDebe <> 0
                                OR GS_cwmovim.MovHaber <> 0
                            )
                            AND MovTipDocRef = \'FV\'
                            GROUP BY
                                MovNumDocRef
                        ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
                        WHERE
                            GS_iw_gsaen.CodAux = \'' . $rut . '\'
                        AND (GS_iw_gsaen.Tipo = \'F\' OR GS_iw_gsaen.Tipo = \'N\')
                        AND GS_iw_gsaen.Estado = \'V\'
                        AND GS_cwmovim_hd.DEBE = GS_cwmovim_hd.HABER
                        AND GS_cwmovim_hd.HABER = GS_iw_gsaen.Total
                        ORDER BY
                            Fecha DESC OFFSET 0 ROWS FETCH NEXT ' . $fact . ' ROWS ONLY
                        UNION ALL
                            SELECT
                                \'MICROBOX\' AS empresa,
                                MB_iw_gsaen.Folio,
                                MB_iw_gsaen.Fecha,
                                MB_iw_gsaen.FechaVenc,
                                DATEDIFF(
                                    DAY,
                                    MB_iw_gsaen.Fecha,
                                    MB_cwmovim_hd.f
                                ) AS DiasPago,
                                MB_cwmovim_hd.HABER AS Abonos,
                                MB_cwmovim_hd.f AS FechaUltAbono
                            FROM
                                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
                            LEFT JOIN (
                                SELECT
                                    SUM (MovDebe) AS DEBE,
                                    SUM (MovHaber) AS HABER,
                                    MovNumDocRef,
                                    MAX (FecPag) AS f
                                FROM
                                    MICROBOX.softland.cwmovim AS MB_cwmovim
                                WHERE
                                    MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                                AND MB_cwmovim.MovGlosa <> \'\'
                                AND MB_cwmovim.CodAux = \'' . $rut . '\'
                                AND MB_cwmovim.CpbFec < GETDATE()
                                AND (
                                    MB_cwmovim.MovDebe <> 0
                                    OR MB_cwmovim.MovHaber <> 0
                                )
                                AND MovTipDocRef = \'FV\'
                                GROUP BY
                                    MovNumDocRef
                            ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
                            WHERE
                                MB_iw_gsaen.CodAux = \'' . $rut . '\'
                            AND (MB_iw_gsaen.Tipo = \'F\' OR MB_iw_gsaen.Tipo = \'N\')
                            AND MB_iw_gsaen.Estado = \'V\'
                            AND MB_cwmovim_hd.DEBE = MB_cwmovim_hd.HABER
                            AND MB_cwmovim_hd.HABER = MB_iw_gsaen.Total
                            ORDER BY
                                Fecha DESC OFFSET 0 ROWS FETCH NEXT ' . $fact . ' ROWS ONLY
                            UNION ALL
                                SELECT
                                    \'PUBLIGRAFIKA\' AS empresa,
                                    TL_iw_gsaen.Folio,
                                    TL_iw_gsaen.Fecha,
                                    TL_iw_gsaen.FechaVenc,
                                    DATEDIFF(
                                        DAY,
                                        TL_iw_gsaen.Fecha,
                                        TL_cwmovim_hd.f
                                    ) AS DiasPago,
                                    TL_cwmovim_hd.HABER AS Abonos,
                                    TL_cwmovim_hd.f AS FechaUltAbono
                                FROM
                                    PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
                                LEFT JOIN (
                                    SELECT
                                        SUM (MovDebe) AS DEBE,
                                        SUM (MovHaber) AS HABER,
                                        MovNumDocRef,
                                        MAX (FecPag) AS f
                                    FROM
                                        PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                                    WHERE
                                        TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                                    AND TL_cwmovim.MovGlosa <> \'\'
                                    AND TL_cwmovim.CodAux = \'' . $rut . '\'
                                    AND TL_cwmovim.CpbFec < GETDATE()
                                    AND (
                                        TL_cwmovim.MovDebe <> 0
                                        OR TL_cwmovim.MovHaber <> 0
                                    )
                                    AND MovTipDocRef = \'FV\'
                                    GROUP BY
                                        MovNumDocRef
                                ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
                                WHERE
                                    TL_iw_gsaen.CodAux = \'' . $rut . '\'
                                AND (TL_iw_gsaen.Tipo = \'F\' OR TL_iw_gsaen.Tipo = \'N\')
                                AND TL_iw_gsaen.Estado = \'V\'
                                AND TL_cwmovim_hd.DEBE = TL_cwmovim_hd.HABER
                                AND TL_cwmovim_hd.HABER = TL_iw_gsaen.Total
                                ORDER BY
                                    Fecha DESC OFFSET 0 ROWS FETCH NEXT ' . $fact . ' ROWS ONLY
                                UNION ALL
                                    SELECT
                                        \'TENSPA\' AS empresa,
                                        TS_iw_gsaen.Folio,
                                        TS_iw_gsaen.Fecha,
                                        TS_iw_gsaen.FechaVenc,
                                        DATEDIFF(
                                            DAY,
                                            TS_iw_gsaen.Fecha,
                                            TS_cwmovim_hd.f
                                        ) AS DiasPago,
                                        TS_cwmovim_hd.HABER AS Abonos,
                                        TS_cwmovim_hd.f AS FechaUltAbono
                                    FROM
                                        TENSPA.softland.iw_gsaen AS TS_iw_gsaen
                                    LEFT JOIN (
                                        SELECT
                                            SUM (MovDebe) AS DEBE,
                                            SUM (MovHaber) AS HABER,
                                            MovNumDocRef,
                                            MAX (FecPag) AS f
                                        FROM
                                            TENSPA.softland.cwmovim AS TS_cwmovim
                                        WHERE
                                            TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                                        AND TS_cwmovim.MovGlosa <> \'\'
                                        AND TS_cwmovim.CodAux = \'' . $rut . '\'
                                        AND TS_cwmovim.CpbFec < GETDATE()
                                        AND (
                                            TS_cwmovim.MovDebe <> 0
                                            OR TS_cwmovim.MovHaber <> 0
                                        )
                                        AND MovTipDocRef = \'FV\'
                                        GROUP BY
                                            MovNumDocRef
                                    ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
                                    WHERE
                                        TS_iw_gsaen.CodAux = \'' . $rut . '\'
                                    AND (TS_iw_gsaen.Tipo = \'F\' OR TS_iw_gsaen.Tipo = \'N\')
                                    AND TS_iw_gsaen.Estado = \'V\'
                                    AND TS_cwmovim_hd.DEBE = TS_cwmovim_hd.HABER
                                    AND TS_cwmovim_hd.HABER = TS_iw_gsaen.Total
                                    ORDER BY
                                        Fecha DESC OFFSET 0 ROWS FETCH NEXT ' . $fact . ' ROWS ONLY
                ) AS prom_pago
                
                WHERE prom_pago.DiasPago IS NOT NULL
        '));

        return $sql;
    }
}
