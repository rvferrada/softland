<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public static function comprobarRutYnombreClienteGrauLtda($nombre, $rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP 1 NomAux
            FROM
                GRAULTDA.softland.cwtauxi
            WHERE
                CodAux = \'' . $rut . '\'
                AND
                NomAux = \'' . $nombre . '\'
            ORDER BY
                CodAux ASC
        '));

        if (count($sql) == 0) {
            return false;
        }

        return true;
    }

    public static function comprobarRutYnombreClienteGrauSpa($nombre, $rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP 1 NomAux
            FROM
                GRAUSPA.softland.cwtauxi
            WHERE
                CodAux = \'' . $rut . '\'
                AND
                NomAux = \'' . $nombre . '\'                
            ORDER BY
                CodAux ASC
        '));

        if (count($sql) == 0) {
            return false;
        }

        return true;
    }

    public static function comprobarRutYnombreClienteMicrobox($nombre, $rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP 1 NomAux
            FROM
                MICROBOX.softland.cwtauxi
            WHERE
                CodAux = \'' . $rut . '\'
                AND
                NomAux = \'' . $nombre . '\'
            ORDER BY
                CodAux ASC
        '));

        if (count($sql) == 0) {
            return false;
        }

        return true;
    }

    public static function comprobarRutYnombreClienteTendenciaLtda($nombre, $rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP 1 NomAux
            FROM
                PUBLIGRAFIKA.softland.cwtauxi
            WHERE
                CodAux = \'' . $rut . '\'
                AND
                NomAux = \'' . $nombre . '\'                
            ORDER BY
                CodAux ASC
        '));

        if (count($sql) == 0) {
            return false;
        }

        return true;
    }

    public static function comprobarRutYnombreClienteTendenciaSpa($nombre, $rut) {

        $sql = \DB::select(\DB::raw('
            SELECT
                TOP 1 NomAux
            FROM
                TENSPA.softland.cwtauxi
            WHERE
                CodAux = \'' . $rut . '\'
            ORDER BY
                CodAux ASC
        '));

        if (count($sql) == 0) {
            return false;
        }

        return true;
    }

    public static function condicionesVenta($rut) {

        $sql = \DB::select(\DB::raw('
            SELECT GR_cwtcvcl.MtoCre, GR_cwtconv.CveDes, 
            \'GRAULTDA\' AS empresa
            FROM GRAULTDA.softland.cwtcvcl AS GR_cwtcvcl
            
            LEFT JOIN GRAULTDA.softland.cwtconv AS GR_cwtconv ON GR_cwtcvcl.ConVta = GR_cwtconv.CveCod
            WHERE GR_cwtcvcl.CodAux = \'' . $rut . '\'
            
            UNION ALL
            
            SELECT GS_cwtcvcl.MtoCre, GS_cwtconv.CveDes, 
            \'GRAUSPA\' AS empresa
            FROM GRAUSPA.softland.cwtcvcl AS GS_cwtcvcl
            
            LEFT JOIN GRAUSPA.softland.cwtconv AS GS_cwtconv ON GS_cwtcvcl.ConVta = GS_cwtconv.CveCod
            WHERE GS_cwtcvcl.CodAux = \'' . $rut . '\'       
                 
            UNION ALL     
                 
            SELECT MB_cwtcvcl.MtoCre, MB_cwtconv.CveDes, 
            \'MICROBOX\' AS empresa
            FROM MICROBOX.softland.cwtcvcl AS MB_cwtcvcl
            
            LEFT JOIN MICROBOX.softland.cwtconv AS MB_cwtconv ON MB_cwtcvcl.ConVta = MB_cwtconv.CveCod
            WHERE MB_cwtcvcl.CodAux = \'' . $rut . '\'        
                           
            UNION ALL
              
            SELECT TL_cwtcvcl.MtoCre, TL_cwtconv.CveDes, 
            \'PUBLIGRAFIKA\' AS empresa
            FROM PUBLIGRAFIKA.softland.cwtcvcl AS TL_cwtcvcl
            
            LEFT JOIN PUBLIGRAFIKA.softland.cwtconv AS TL_cwtconv ON TL_cwtcvcl.ConVta = TL_cwtconv.CveCod
            WHERE TL_cwtcvcl.CodAux = \'' . $rut . '\'          
                    
            UNION ALL
            
            SELECT TS_cwtcvcl.MtoCre, TS_cwtconv.CveDes, 
            \'TENSPA\' AS empresa
            FROM TENSPA.softland.cwtcvcl AS TS_cwtcvcl
            
            LEFT JOIN TENSPA.softland.cwtconv AS TS_cwtconv ON TS_cwtcvcl.ConVta = TS_cwtconv.CveCod
            WHERE TS_cwtcvcl.CodAux = \'' . $rut . '\'                       
        '));

        return $sql;
    }

    public static function productosDeCliente($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                GR_iw_tsubgr.CodSubGr,
                GR_iw_tsubgr.DesSubGr,
                GR_iw_tprod.CodProd,
                GR_iw_tprod.DesProd,
                GR_iw_tprod.CodUMed,
                GR_iw_costop.CostoUnitario,
                GR_iw_costop.Stock,
                \'GRAULTDA\' AS empresa
            FROM
                GRAULTDA.softland.iw_tsubgr AS GR_iw_tsubgr
            INNER JOIN GRAULTDA.softland.iw_tprod AS GR_iw_tprod ON GR_iw_tsubgr.CodSubGr = GR_iw_tprod.CodSubGr
            
            LEFT JOIN (
                SELECT
                    CodProd,
                    MAX (Fecha) AS Fecha
                FROM
                    GRAULTDA.softland.iw_costop AS GR_iw_costop
                GROUP BY
                    CodProd
            ) AS GR_iw_costop_f ON GR_iw_tprod.CodProd = GR_iw_costop_f.CodProd
            
            LEFT JOIN GRAULTDA.softland.iw_costop AS GR_iw_costop ON GR_iw_costop_f.CodProd = GR_iw_costop.CodProd
            AND GR_iw_costop_f.Fecha = GR_iw_costop.Fecha
            
            WHERE
                GR_iw_tsubgr.DesSubGr = \'' . $nombre . '\'
                
            UNION ALL
            
            SELECT
                GS_iw_tsubgr.CodSubGr,
                GS_iw_tsubgr.DesSubGr,
                GS_iw_tprod.CodProd,
                GS_iw_tprod.DesProd,
                GS_iw_tprod.CodUMed,
                GS_iw_costop.CostoUnitario,
                GS_iw_costop.Stock,
                \'GRAUSPA\' AS empresa
            FROM
                GRAUSPA.softland.iw_tsubgr AS GS_iw_tsubgr
            INNER JOIN GRAUSPA.softland.iw_tprod AS GS_iw_tprod ON GS_iw_tsubgr.CodSubGr = GS_iw_tprod.CodSubGr
            
            LEFT JOIN (
                SELECT
                    CodProd,
                    MAX (Fecha) AS Fecha
                FROM
                    GRAUSPA.softland.iw_costop AS GS_iw_costop
                GROUP BY
                    CodProd
            ) AS GS_iw_costop_f ON GS_iw_tprod.CodProd = GS_iw_costop_f.CodProd
            
            LEFT JOIN GRAUSPA.softland.iw_costop AS GS_iw_costop ON GS_iw_costop_f.CodProd = GS_iw_costop.CodProd
            AND GS_iw_costop_f.Fecha = GS_iw_costop.Fecha
            
            WHERE
                GS_iw_tsubgr.DesSubGr = \'' . $nombre . '\'
            
            UNION ALL
            
            SELECT
                MB_iw_tsubgr.CodSubGr,
                MB_iw_tsubgr.DesSubGr,
                MB_iw_tprod.CodProd,
                MB_iw_tprod.DesProd,
                MB_iw_tprod.CodUMed,
                MB_iw_costop.CostoUnitario,
                MB_iw_costop.Stock,
                \'MICROBOX\' AS empresa
            FROM
                MICROBOX.softland.iw_tsubgr AS MB_iw_tsubgr
            INNER JOIN MICROBOX.softland.iw_tprod AS MB_iw_tprod ON MB_iw_tsubgr.CodSubGr = MB_iw_tprod.CodSubGr
            
            LEFT JOIN (
                SELECT
                    CodProd,
                    MAX (Fecha) AS Fecha
                FROM
                    MICROBOX.softland.iw_costop AS MB_iw_costop
                GROUP BY
                    CodProd
            ) AS MB_iw_costop_f ON MB_iw_tprod.CodProd = MB_iw_costop_f.CodProd
            
            LEFT JOIN MICROBOX.softland.iw_costop AS MB_iw_costop ON MB_iw_costop_f.CodProd = MB_iw_costop.CodProd
            AND MB_iw_costop_f.Fecha = MB_iw_costop.Fecha
            
            WHERE
                MB_iw_tsubgr.DesSubGr = \'' . $nombre . '\'
            
            UNION ALL
            
            SELECT
                TL_iw_tsubgr.CodSubGr,
                TL_iw_tsubgr.DesSubGr,
                TL_iw_tprod.CodProd,
                TL_iw_tprod.DesProd,
                TL_iw_tprod.CodUMed,
                TL_iw_costop.CostoUnitario,
                TL_iw_costop.Stock,
                \'PUBLIGRAFIKA\' AS empresa
            FROM
                PUBLIGRAFIKA.softland.iw_tsubgr AS TL_iw_tsubgr
            INNER JOIN PUBLIGRAFIKA.softland.iw_tprod AS TL_iw_tprod ON TL_iw_tsubgr.CodSubGr = TL_iw_tprod.CodSubGr
            
            LEFT JOIN (
                SELECT
                    CodProd,
                    MAX (Fecha) AS Fecha
                FROM
                    PUBLIGRAFIKA.softland.iw_costop AS TL_iw_costop
                GROUP BY
                    CodProd
            ) AS TL_iw_costop_f ON TL_iw_tprod.CodProd = TL_iw_costop_f.CodProd
            
            LEFT JOIN PUBLIGRAFIKA.softland.iw_costop AS TL_iw_costop ON TL_iw_costop_f.CodProd = TL_iw_costop.CodProd
            AND TL_iw_costop_f.Fecha = TL_iw_costop.Fecha
            
            WHERE
                TL_iw_tsubgr.DesSubGr = \'' . $nombre . '\'
            
            UNION ALL
            
            SELECT
                TS_iw_tsubgr.CodSubGr,
                TS_iw_tsubgr.DesSubGr,
                TS_iw_tprod.CodProd,
                TS_iw_tprod.DesProd,
                TS_iw_tprod.CodUMed,
                TS_iw_costop.CostoUnitario,
                TS_iw_costop.Stock,
                \'TENSPA\' AS empresa
            FROM
                TENSPA.softland.iw_tsubgr AS TS_iw_tsubgr
            INNER JOIN TENSPA.softland.iw_tprod AS TS_iw_tprod ON TS_iw_tsubgr.CodSubGr = TS_iw_tprod.CodSubGr
            
            LEFT JOIN (
                SELECT
                    CodProd,
                    MAX (Fecha) AS Fecha
                FROM
                    TENSPA.softland.iw_costop AS TS_iw_costop
                GROUP BY
                    CodProd
            ) AS TS_iw_costop_f ON TS_iw_tprod.CodProd = TS_iw_costop_f.CodProd
            
            LEFT JOIN TENSPA.softland.iw_costop AS TS_iw_costop ON TS_iw_costop_f.CodProd = TS_iw_costop.CodProd
            AND TS_iw_costop_f.Fecha = TS_iw_costop.Fecha
            
            WHERE
                TS_iw_tsubgr.DesSubGr = \'' . $nombre . '\'
            ORDER BY CodProd, Stock ASC
        '));

        return $sql;
    }
}
