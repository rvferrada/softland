<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Vendedor extends Model
{
    public static function nombreDeVendedores() {
        $sql = \DB::select(\DB::raw('
            SELECT DISTINCT VenDes FROM
            
            (SELECT *, \'GRAULTDA\' AS Empresa FROM GRAULTDA.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'GRAUSPA\' AS Empresa FROM GRAUSPA.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'MICROBOX\' AS Empresa FROM MICROBOX.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'PUBLIGRAFIKA\' AS Empresa FROM PUBLIGRAFIKA.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'TENSPA\' AS Empresa FROM TENSPA.softland.cwtvend) AS Vendedores
            
            ORDER BY VenDes ASC        
        '));

        return $sql;
    }

    public static function comprobarNombreVendedorEmpresas($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT DISTINCT VenDes FROM
            
            (SELECT *, \'GRAULTDA\' AS Empresa FROM GRAULTDA.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'GRAUSPA\' AS Empresa FROM GRAUSPA.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'MICROBOX\' AS Empresa FROM MICROBOX.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'PUBLIGRAFIKA\' AS Empresa FROM PUBLIGRAFIKA.softland.cwtvend
            
            UNION ALL
            
            SELECT *, \'TENSPA\' AS Empresa FROM TENSPA.softland.cwtvend) AS Vendedores
            
            WHERE VenDes = \'' . $nombre . '\'
            
            ORDER BY VenDes ASC        
        '));

        if (count($sql) == 0) {
            return false;
        }

        return true;
    }

    public static function totalFacturadoHaceDosMeses($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS total,
                \'GRAULTDA\' AS empresa
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
                
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod 
            
            WHERE
                GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (GR_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 2, GETDATE()))
            AND YEAR (GR_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 2, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'GRAUSPA\' AS empresa
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
                
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod 
            
            WHERE
                GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (GS_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 2, GETDATE()))
            AND YEAR (GS_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 2, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'MICROBOX\' AS empresa
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
                
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod 
            
            WHERE
                MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (MB_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 2, GETDATE()))
            AND YEAR (MB_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 2, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'PUBLIGRAFIKA\' AS empresa
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
                
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod 
            
            WHERE
                TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (TL_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 2, GETDATE()))
            AND YEAR (TL_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 2, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'TENSPA\' AS empresa
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
              
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod 
            
            WHERE
                TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (TS_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 2, GETDATE()))
            AND YEAR (TS_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 2, GETDATE()))
        '));

        return $sql;
    }

    public static function totalFacturadoHaceUnMes($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (SubTotal) AS total,
                \'GRAULTDA\' AS empresa
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
                
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod 
            
            WHERE
                GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (GR_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 1, GETDATE()))
            AND YEAR (GR_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 1, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'GRAUSPA\' AS empresa
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
                
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod 
            
            WHERE
                GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (GS_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 1, GETDATE()))
            AND YEAR (GS_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 1, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'MICROBOX\' AS empresa
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
                
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod 
            
            WHERE
                MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (MB_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 1, GETDATE()))
            AND YEAR (MB_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 1, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'PUBLIGRAFIKA\' AS empresa
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
                
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod 
            
            WHERE
                TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (TL_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 1, GETDATE()))
            AND YEAR (TL_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 1, GETDATE()))
            
            UNION ALL
            
            SELECT
                SUM (SubTotal) AS total,
                \'TENSPA\' AS empresa
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
              
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod 
            
            WHERE
                TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH (TS_iw_gsaen.Fecha) = MONTH(DATEADD(MONTH, - 1, GETDATE()))
            AND YEAR (TS_iw_gsaen.Fecha) = YEAR(DATEADD(MONTH, - 1, GETDATE()))        
        '));

        return $sql;
    }

    public static function totalFacturadoActual($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                \'GRAULTDA\' AS empresa
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
                
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod 
            
            WHERE
                GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH(GR_iw_gsaen.Fecha) = MONTH(GETDATE())
            AND YEAR(GR_iw_gsaen.Fecha) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT
                SUM (subTotal) AS Total,
                \'GRAUSPA\' AS empresa
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
                
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod 
            
            WHERE
                GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH(GS_iw_gsaen.Fecha) = MONTH(GETDATE())
            AND YEAR(GS_iw_gsaen.Fecha) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT
                SUM (subTotal) AS Total,
                \'MICROBOX\' AS empresa
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
                
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod 
            
            WHERE
                MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH(MB_iw_gsaen.Fecha) = MONTH(GETDATE())
            AND YEAR(MB_iw_gsaen.Fecha) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT
                SUM (subTotal) AS Total,
                \'PUBLIGRAFIKA\' AS empresa
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
                
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod 
            
            WHERE
                TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH(TL_iw_gsaen.Fecha) = MONTH(GETDATE())
            AND YEAR(TL_iw_gsaen.Fecha) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT
                SUM (subTotal) AS Total,
                \'TENSPA\' AS empresa
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
                
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod 
            
            WHERE
                TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            AND MONTH(TS_iw_gsaen.Fecha) = MONTH(GETDATE())
            AND YEAR(TS_iw_gsaen.Fecha) = YEAR(GETDATE())
        '));

        return $sql;
    }

    public static function totalCobradoHaceUnMes($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT 
                \'GRAULTDA\' AS empresa,
                SUM(GR_iw_gsaen.SubTotal) AS Abonos
            
            FROM GRAULTDA.softland.cwtvend AS GR_cwtvend
            
            INNER JOIN GRAULTDA.softland.cwtauxven AS GR_cwtauxven ON GR_cwtvend.VenCod = GR_cwtauxven.VenCod
            
            INNER JOIN GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen ON GR_cwtauxven.CodAux = GR_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    GRAULTDA.softland.cwmovim AS GR_cwmovim
                WHERE
                    GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND GR_cwmovim.MovGlosa <> \'\'
                AND GR_cwmovim.CpbFec < GETDATE()
                AND (
                    GR_cwmovim.MovDebe <> 0
                    OR GR_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
            
            WHERE GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND GR_iw_gsaen.Tipo = \'F\'
            AND GR_iw_gsaen.Estado = \'V\'
            AND GR_cwmovim_hd.f > DATEADD(MONTH, -1, GETDATE())
            AND GR_cwmovim_hd.f < DATEADD(MONTH, 1, GETDATE())
            AND MONTH(GR_cwmovim_hd.f) != MONTH(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'GRAUSPA\' AS empresa,
                SUM(GS_iw_gsaen.SubTotal) AS Abonos
            
            FROM GRAUSPA.softland.cwtvend AS GS_cwtvend
            
            INNER JOIN GRAUSPA.softland.cwtauxven AS GS_cwtauxven ON GS_cwtvend.VenCod = GS_cwtauxven.VenCod
            
            INNER JOIN GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen ON GS_cwtauxven.CodAux = GS_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    GRAUSPA.softland.cwmovim AS GS_cwmovim
                WHERE
                    GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND GS_cwmovim.MovGlosa <> \'\'
                AND GS_cwmovim.CpbFec < GETDATE()
                AND (
                    GS_cwmovim.MovDebe <> 0
                    OR GS_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
            
            WHERE GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND GS_iw_gsaen.Tipo = \'F\'
            AND GS_iw_gsaen.Estado = \'V\'
            AND GS_cwmovim_hd.f > DATEADD(MONTH, -1, GETDATE())
            AND GS_cwmovim_hd.f < DATEADD(MONTH, 1, GETDATE())
            AND MONTH(GS_cwmovim_hd.f) != MONTH(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'MICROBOX\' AS empresa,
                SUM(MB_iw_gsaen.SubTotal) AS Abonos
            
            FROM MICROBOX.softland.cwtvend AS MB_cwtvend
            
            INNER JOIN MICROBOX.softland.cwtauxven AS MB_cwtauxven ON MB_cwtvend.VenCod = MB_cwtauxven.VenCod
            
            INNER JOIN MICROBOX.softland.iw_gsaen AS MB_iw_gsaen ON MB_cwtauxven.CodAux = MB_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    MICROBOX.softland.cwmovim AS MB_cwmovim
                WHERE
                    MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND MB_cwmovim.MovGlosa <> \'\'
                AND MB_cwmovim.CpbFec < GETDATE()
                AND (
                    MB_cwmovim.MovDebe <> 0
                    OR MB_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
            
            WHERE MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND MB_iw_gsaen.Tipo = \'F\'
            AND MB_iw_gsaen.Estado = \'V\'
            AND MB_cwmovim_hd.f > DATEADD(MONTH, -1, GETDATE())
            AND MB_cwmovim_hd.f < DATEADD(MONTH, 1, GETDATE())
            AND MONTH(MB_cwmovim_hd.f) != MONTH(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'PUBLIGRAFIKA\' AS empresa,
                SUM(TL_iw_gsaen.SubTotal) AS Abonos
            
            FROM PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend
            
            INNER JOIN PUBLIGRAFIKA.softland.cwtauxven AS TL_cwtauxven ON TL_cwtvend.VenCod = TL_cwtauxven.VenCod
            
            INNER JOIN PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen ON TL_cwtauxven.CodAux = TL_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                WHERE
                    TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND TL_cwmovim.MovGlosa <> \'\'
                AND TL_cwmovim.CpbFec < GETDATE()
                AND (
                    TL_cwmovim.MovDebe <> 0
                    OR TL_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
            
            WHERE TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND TL_iw_gsaen.Tipo = \'F\'
            AND TL_iw_gsaen.Estado = \'V\'
            AND TL_cwmovim_hd.f > DATEADD(MONTH, -1, GETDATE())
            AND TL_cwmovim_hd.f < DATEADD(MONTH, 1, GETDATE())
            AND MONTH(TL_cwmovim_hd.f) != MONTH(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'TENSPA\' AS empresa,
                SUM(TS_iw_gsaen.SubTotal) AS Abonos
            
            FROM TENSPA.softland.cwtvend AS TS_cwtvend
            
            INNER JOIN TENSPA.softland.cwtauxven AS TS_cwtauxven ON TS_cwtvend.VenCod = TS_cwtauxven.VenCod
            
            INNER JOIN TENSPA.softland.iw_gsaen AS TS_iw_gsaen ON TS_cwtauxven.CodAux = TS_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    TENSPA.softland.cwmovim AS TS_cwmovim
                WHERE
                    TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND TS_cwmovim.MovGlosa <> \'\'
                AND TS_cwmovim.CpbFec < GETDATE()
                AND (
                    TS_cwmovim.MovDebe <> 0
                    OR TS_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
            
            WHERE TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND TS_iw_gsaen.Tipo = \'F\'
            AND TS_iw_gsaen.Estado = \'V\'
            AND TS_cwmovim_hd.f > DATEADD(MONTH, -1, GETDATE())
            AND TS_cwmovim_hd.f < DATEADD(MONTH, 1, GETDATE())
            AND MONTH(TS_cwmovim_hd.f) != MONTH(GETDATE())
        '));

        return $sql;
    }

    public static function totalCobradoMesActual($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT 
                \'GRAULTDA\' AS empresa,
                SUM(GR_iw_gsaen.SubTotal) AS Abonos
            
            FROM GRAULTDA.softland.cwtvend AS GR_cwtvend
            
            INNER JOIN GRAULTDA.softland.cwtauxven AS GR_cwtauxven ON GR_cwtvend.VenCod = GR_cwtauxven.VenCod
            
            INNER JOIN GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen ON GR_cwtauxven.CodAux = GR_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    GRAULTDA.softland.cwmovim AS GR_cwmovim
                WHERE
                    GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND GR_cwmovim.MovGlosa <> \'\'
                AND GR_cwmovim.CpbFec < GETDATE()
                AND (
                    GR_cwmovim.MovDebe <> 0
                    OR GR_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
            
            WHERE GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND GR_iw_gsaen.Tipo = \'F\'
            AND GR_iw_gsaen.Estado = \'V\'
            AND MONTH(GR_cwmovim_hd.f) = MONTH(GETDATE())
            AND YEAR(GR_cwmovim_hd.f) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'GRAUSPA\' AS empresa,
                SUM(GS_iw_gsaen.SubTotal) AS Abonos
            
            FROM GRAUSPA.softland.cwtvend AS GS_cwtvend
            
            INNER JOIN GRAUSPA.softland.cwtauxven AS GS_cwtauxven ON GS_cwtvend.VenCod = GS_cwtauxven.VenCod
            
            INNER JOIN GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen ON GS_cwtauxven.CodAux = GS_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    GRAUSPA.softland.cwmovim AS GS_cwmovim
                WHERE
                    GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND GS_cwmovim.MovGlosa <> \'\'
                AND GS_cwmovim.CpbFec < GETDATE()
                AND (
                    GS_cwmovim.MovDebe <> 0
                    OR GS_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
            
            WHERE GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND GS_iw_gsaen.Tipo = \'F\'
            AND GS_iw_gsaen.Estado = \'V\'
            AND MONTH(GS_cwmovim_hd.f) = MONTH(GETDATE())
            AND YEAR(GS_cwmovim_hd.f) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'MICROBOX\' AS empresa,
                SUM(MB_iw_gsaen.SubTotal) AS Abonos
            
            FROM MICROBOX.softland.cwtvend AS MB_cwtvend
            
            INNER JOIN MICROBOX.softland.cwtauxven AS MB_cwtauxven ON MB_cwtvend.VenCod = MB_cwtauxven.VenCod
            
            INNER JOIN MICROBOX.softland.iw_gsaen AS MB_iw_gsaen ON MB_cwtauxven.CodAux = MB_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    MICROBOX.softland.cwmovim AS MB_cwmovim
                WHERE
                    MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND MB_cwmovim.MovGlosa <> \'\'
                AND MB_cwmovim.CpbFec < GETDATE()
                AND (
                    MB_cwmovim.MovDebe <> 0
                    OR MB_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
            
            WHERE MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND MB_iw_gsaen.Tipo = \'F\'
            AND MB_iw_gsaen.Estado = \'V\'
            AND MONTH(MB_cwmovim_hd.f) = MONTH(GETDATE())
            AND YEAR(MB_cwmovim_hd.f) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'PUBLIGRAFIKA\' AS empresa,
                SUM(TL_iw_gsaen.SubTotal) AS Abonos
            
            FROM PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend
            
            INNER JOIN PUBLIGRAFIKA.softland.cwtauxven AS TL_cwtauxven ON TL_cwtvend.VenCod = TL_cwtauxven.VenCod
            
            INNER JOIN PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen ON TL_cwtauxven.CodAux = TL_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                WHERE
                    TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND TL_cwmovim.MovGlosa <> \'\'
                AND TL_cwmovim.CpbFec < GETDATE()
                AND (
                    TL_cwmovim.MovDebe <> 0
                    OR TL_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
            
            WHERE TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND TL_iw_gsaen.Tipo = \'F\'
            AND TL_iw_gsaen.Estado = \'V\'
            AND MONTH(TL_cwmovim_hd.f) = MONTH(GETDATE())
            AND YEAR(TL_cwmovim_hd.f) = YEAR(GETDATE())
            
            UNION ALL
            
            SELECT 
                \'TENSPA\' AS empresa,
                SUM(TS_iw_gsaen.SubTotal) AS Abonos
            
            FROM TENSPA.softland.cwtvend AS TS_cwtvend
            
            INNER JOIN TENSPA.softland.cwtauxven AS TS_cwtauxven ON TS_cwtvend.VenCod = TS_cwtauxven.VenCod
            
            INNER JOIN TENSPA.softland.iw_gsaen AS TS_iw_gsaen ON TS_cwtauxven.CodAux = TS_iw_gsaen.CodAux
            
            INNER JOIN (
                SELECT
                    SUM (MovDebe) AS DEBE,
                    SUM (MovHaber) AS HABER,
                    MovNumDocRef,
                    MAX (FecPag) AS f
                FROM
                    TENSPA.softland.cwmovim AS TS_cwmovim
                WHERE
                    TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                AND TS_cwmovim.MovGlosa <> \'\'
                AND TS_cwmovim.CpbFec < GETDATE()
                AND (
                    TS_cwmovim.MovDebe <> 0
                    OR TS_cwmovim.MovHaber <> 0
                )
                AND MovTipDocRef = \'FV\'
                GROUP BY
                    MovNumDocRef
            ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
            
            WHERE TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND TS_iw_gsaen.Tipo = \'F\'
            AND TS_iw_gsaen.Estado = \'V\'
            AND MONTH(TS_cwmovim_hd.f) = MONTH(GETDATE())
            AND YEAR(TS_cwmovim_hd.f) = YEAR(GETDATE())
        '));

        return $sql;
    }

    public static function totalVencidoYporVencer($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT SUM(SubTotal) AS Total, estadoFactura, empresa
            FROM
                (SELECT
                    CASE
                WHEN GR_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                 \'GRAULTDA\' AS empresa,
                 GR_iw_gsaen.Folio,
                 GR_iw_gsaen.Total,
                 GR_iw_gsaen.SubTotal,
                 GR_cwmovim_hd.DEBE,
                 GR_cwmovim_hd.HABER,
                 GR_iw_gsaen.FechaVenc
                FROM
                    GRAULTDA.softland.cwtvend AS GR_cwtvend
                INNER JOIN GRAULTDA.softland.cwtauxven AS GR_cwtauxven ON GR_cwtvend.VenCod = GR_cwtauxven.VenCod
                INNER JOIN GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen ON GR_cwtauxven.CodAux = GR_iw_gsaen.CodAux
                INNER JOIN (
                    SELECT
                        SUM (MovDebe) AS DEBE,
                        SUM (MovHaber) AS HABER,
                        MovNumDocRef,
                        MAX (FecPag) AS f
                    FROM
                        GRAULTDA.softland.cwmovim AS GR_cwmovim
                    WHERE
                        GR_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                    AND GR_cwmovim.MovGlosa <> \'\'
                    AND GR_cwmovim.CpbFec < GETDATE()
                    AND (
                        GR_cwmovim.MovDebe <> 0
                        OR GR_cwmovim.MovHaber <> 0
                    )
                    AND MovTipDocRef = \'FV\'
                    GROUP BY
                        MovNumDocRef
                ) AS GR_cwmovim_hd ON GR_iw_gsaen.Folio = GR_cwmovim_hd.MovNumDocRef
                WHERE
                    GR_cwtvend.VenDes = \'' . $nombre . '\'
                AND (GR_iw_gsaen.Tipo = \'F\' OR GR_iw_gsaen.Tipo = \'N\')
                AND GR_iw_gsaen.Estado = \'V\'
                AND HABER < DEBE
                AND DEBE = GR_iw_gsaen.Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa 
            
            UNION ALL
            
            SELECT SUM(SubTotal) AS Total, estadoFactura, empresa
            FROM
                (SELECT
                    CASE
                WHEN GS_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                 \'GRAUSPA\' AS empresa,
                 GS_iw_gsaen.Folio,
                 GS_iw_gsaen.Total,
                 GS_iw_gsaen.SubTotal,
                 GS_cwmovim_hd.DEBE,
                 GS_cwmovim_hd.HABER,
                 GS_iw_gsaen.FechaVenc
                FROM
                    GRAUSPA.softland.cwtvend AS GS_cwtvend
                INNER JOIN GRAUSPA.softland.cwtauxven AS GS_cwtauxven ON GS_cwtvend.VenCod = GS_cwtauxven.VenCod
                INNER JOIN GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen ON GS_cwtauxven.CodAux = GS_iw_gsaen.CodAux
                INNER JOIN (
                    SELECT
                        SUM (MovDebe) AS DEBE,
                        SUM (MovHaber) AS HABER,
                        MovNumDocRef,
                        MAX (FecPag) AS f
                    FROM
                        GRAUSPA.softland.cwmovim AS GS_cwmovim
                    WHERE
                        GS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                    AND GS_cwmovim.MovGlosa <> \'\'
                    AND GS_cwmovim.CpbFec < GETDATE()
                    AND (
                        GS_cwmovim.MovDebe <> 0
                        OR GS_cwmovim.MovHaber <> 0
                    )
                    AND MovTipDocRef = \'FV\'
                    GROUP BY
                        MovNumDocRef
                ) AS GS_cwmovim_hd ON GS_iw_gsaen.Folio = GS_cwmovim_hd.MovNumDocRef
                WHERE
                    GS_cwtvend.VenDes = \'' . $nombre . '\'
                AND (GS_iw_gsaen.Tipo = \'F\' OR GS_iw_gsaen.Tipo = \'N\')
                AND GS_iw_gsaen.Estado = \'V\'
                AND HABER < DEBE
                AND DEBE = GS_iw_gsaen.Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa 
            
            UNION ALL
            
            SELECT SUM(SubTotal) AS Total, estadoFactura, empresa
            FROM
                (SELECT
                    CASE
                WHEN MB_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                 \'MICROBOX\' AS empresa,
                 MB_iw_gsaen.Folio,
                 MB_iw_gsaen.Total,
                 MB_iw_gsaen.SubTotal,
                 MB_cwmovim_hd.DEBE,
                 MB_cwmovim_hd.HABER,
                 MB_iw_gsaen.FechaVenc
                FROM
                    MICROBOX.softland.cwtvend AS MB_cwtvend
                INNER JOIN MICROBOX.softland.cwtauxven AS MB_cwtauxven ON MB_cwtvend.VenCod = MB_cwtauxven.VenCod
                INNER JOIN MICROBOX.softland.iw_gsaen AS MB_iw_gsaen ON MB_cwtauxven.CodAux = MB_iw_gsaen.CodAux
                INNER JOIN (
                    SELECT
                        SUM (MovDebe) AS DEBE,
                        SUM (MovHaber) AS HABER,
                        MovNumDocRef,
                        MAX (FecPag) AS f
                    FROM
                        MICROBOX.softland.cwmovim AS MB_cwmovim
                    WHERE
                        MB_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                    AND MB_cwmovim.MovGlosa <> \'\'
                    AND MB_cwmovim.CpbFec < GETDATE()
                    AND (
                        MB_cwmovim.MovDebe <> 0
                        OR MB_cwmovim.MovHaber <> 0
                    )
                    AND MovTipDocRef = \'FV\'
                    GROUP BY
                        MovNumDocRef
                ) AS MB_cwmovim_hd ON MB_iw_gsaen.Folio = MB_cwmovim_hd.MovNumDocRef
                WHERE
                    MB_cwtvend.VenDes = \'' . $nombre . '\'
                AND (MB_iw_gsaen.Tipo = \'F\' OR MB_iw_gsaen.Tipo = \'N\')
                AND MB_iw_gsaen.Estado = \'V\'
                AND HABER < DEBE
                AND DEBE = MB_iw_gsaen.Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa 
            
            UNION ALL
            
            SELECT SUM(SubTotal) AS Total, estadoFactura, empresa
            FROM
                (SELECT
                    CASE
                WHEN TL_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                 \'PUBLIGRAFIKA\' AS empresa,
                 TL_iw_gsaen.Folio,
                 TL_iw_gsaen.Total,
                 TL_iw_gsaen.SubTotal,
                 TL_cwmovim_hd.DEBE,
                 TL_cwmovim_hd.HABER,
                 TL_iw_gsaen.FechaVenc
                FROM
                    PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend
                INNER JOIN PUBLIGRAFIKA.softland.cwtauxven AS TL_cwtauxven ON TL_cwtvend.VenCod = TL_cwtauxven.VenCod
                INNER JOIN PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen ON TL_cwtauxven.CodAux = TL_iw_gsaen.CodAux
                INNER JOIN (
                    SELECT
                        SUM (MovDebe) AS DEBE,
                        SUM (MovHaber) AS HABER,
                        MovNumDocRef,
                        MAX (FecPag) AS f
                    FROM
                        PUBLIGRAFIKA.softland.cwmovim AS TL_cwmovim
                    WHERE
                        TL_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                    AND TL_cwmovim.MovGlosa <> \'\'
                    AND TL_cwmovim.CpbFec < GETDATE()
                    AND (
                        TL_cwmovim.MovDebe <> 0
                        OR TL_cwmovim.MovHaber <> 0
                    )
                    AND MovTipDocRef = \'FV\'
                    GROUP BY
                        MovNumDocRef
                ) AS TL_cwmovim_hd ON TL_iw_gsaen.Folio = TL_cwmovim_hd.MovNumDocRef
                WHERE
                    TL_cwtvend.VenDes = \'' . $nombre . '\'
                AND (TL_iw_gsaen.Tipo = \'F\' OR TL_iw_gsaen.Tipo = \'N\') 
                AND TL_iw_gsaen.Estado = \'V\'
                AND HABER < DEBE
                AND DEBE = TL_iw_gsaen.Total) AS estadoFactura
            
            GROUP BY estadoFactura, empresa 
            
            UNION ALL
            
            SELECT SUM(SubTotal) AS Total, estadoFactura, empresa
            FROM
                (SELECT
                    CASE
                WHEN TS_iw_gsaen.FechaVenc < GETDATE() THEN
                    \'VENCIDO\'
                ELSE
                    \'POR VENCER\'
                END AS estadoFactura,
                 \'TENSPA\' AS empresa,
                 TS_iw_gsaen.Folio,
                 TS_iw_gsaen.Total,
                 TS_iw_gsaen.SubTotal,
                 TS_cwmovim_hd.DEBE,
                 TS_cwmovim_hd.HABER,
                 TS_iw_gsaen.FechaVenc
                FROM
                    TENSPA.softland.cwtvend AS TS_cwtvend
                INNER JOIN TENSPA.softland.cwtauxven AS TS_cwtauxven ON TS_cwtvend.VenCod = TS_cwtauxven.VenCod
                INNER JOIN TENSPA.softland.iw_gsaen AS TS_iw_gsaen ON TS_cwtauxven.CodAux = TS_iw_gsaen.CodAux
                INNER JOIN (
                    SELECT
                        SUM (MovDebe) AS DEBE,
                        SUM (MovHaber) AS HABER,
                        MovNumDocRef,
                        MAX (FecPag) AS f
                    FROM
                        TENSPA.softland.cwmovim AS TS_cwmovim
                    WHERE
                        TS_cwmovim.MovGlosa <> \'Movimiento de Apertura\'
                    AND TS_cwmovim.MovGlosa <> \'\'
                    AND TS_cwmovim.CpbFec < GETDATE()
                    AND (
                        TS_cwmovim.MovDebe <> 0
                        OR TS_cwmovim.MovHaber <> 0
                    )
                    AND MovTipDocRef = \'FV\'
                    GROUP BY
                        MovNumDocRef
                ) AS TS_cwmovim_hd ON TS_iw_gsaen.Folio = TS_cwmovim_hd.MovNumDocRef
                WHERE
                    TS_cwtvend.VenDes = \'' . $nombre . '\'
                AND (TS_iw_gsaen.Tipo = \'F\' OR TS_iw_gsaen.Tipo = \'N\')
                AND TS_iw_gsaen.Estado = \'V\'
                AND HABER < DEBE
                AND DEBE = TS_iw_gsaen.Total) AS estadoFactura
                       
                GROUP BY estadoFactura, empresa                       
        '));

        return $sql;
    }

    /**
     * @param $nombre
     * @return mixed
     * No aplica descuentos por notas de venta en Tipo = \'F\'
     */
    public static function totalUltimosDoceMeses($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM(GR_iw_gsaen.Total) AS Total,
                MONTH(GR_iw_gsaen.Fecha) AS FechaMes,
                YEAR(GR_iw_gsaen.Fecha) AS FechaAno,
                \'GRAULTDA\' AS empresa
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
            
            LEFT JOIN GRAULTDA.softland.cwtauxven AS GR_cwtauxven ON GR_iw_gsaen.CodAux = GR_cwtauxven.CodAux
            
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_cwtauxven.VenCod = GR_cwtvend.VenCod
            
            WHERE
                GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND GR_iw_gsaen.Tipo = \'F\'
            AND GR_iw_gsaen.Estado = \'V\'
            AND GR_iw_gsaen.Fecha > DATEADD(month, -12, GETDATE())
            
            GROUP BY MONTH(GR_iw_gsaen.Fecha), YEAR(GR_iw_gsaen.Fecha)
                      
            UNION ALL
            
            SELECT
                SUM(GS_iw_gsaen.Total) AS Total,
                MONTH(GS_iw_gsaen.Fecha) AS FechaMes,
                YEAR(GS_iw_gsaen.Fecha) AS FechaAno,
                \'GRAUSPA\' AS empresa
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
            
            LEFT JOIN GRAUSPA.softland.cwtauxven AS GS_cwtauxven ON GS_iw_gsaen.CodAux = GS_cwtauxven.CodAux
            
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_cwtauxven.VenCod = GS_cwtvend.VenCod
            
            WHERE
                GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND GS_iw_gsaen.Tipo = \'F\'
            AND GS_iw_gsaen.Estado = \'V\'
            AND GS_iw_gsaen.Fecha > DATEADD(month, -12, GETDATE())
            
            GROUP BY MONTH(GS_iw_gsaen.Fecha), YEAR(GS_iw_gsaen.Fecha)
                        
            UNION ALL
            
            SELECT
                SUM(MB_iw_gsaen.Total) AS Total,
                MONTH(MB_iw_gsaen.Fecha) AS FechaMes,
                YEAR(MB_iw_gsaen.Fecha) AS FechaAno,
                \'MICROBOX\' AS empresa
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
            
            LEFT JOIN MICROBOX.softland.cwtauxven AS MB_cwtauxven ON MB_iw_gsaen.CodAux = MB_cwtauxven.CodAux
            
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_cwtauxven.VenCod = MB_cwtvend.VenCod
            
            WHERE
                MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND MB_iw_gsaen.Tipo = \'F\'
            AND MB_iw_gsaen.Estado = \'V\'
            AND MB_iw_gsaen.Fecha > DATEADD(month, -12, GETDATE())
            
            GROUP BY MONTH(MB_iw_gsaen.Fecha), YEAR(MB_iw_gsaen.Fecha)
                        
            UNION ALL
            
            SELECT
                SUM(TL_iw_gsaen.Total) AS Total,
                MONTH(TL_iw_gsaen.Fecha) AS FechaMes,
                YEAR(TL_iw_gsaen.Fecha) AS FechaAno,
                \'PUBLIGRAFIKA\' AS empresa
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            
            LEFT JOIN PUBLIGRAFIKA.softland.cwtauxven AS TL_cwtauxven ON TL_iw_gsaen.CodAux = TL_cwtauxven.CodAux
            
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_cwtauxven.VenCod = TL_cwtvend.VenCod
            
            WHERE
                TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND TL_iw_gsaen.Tipo = \'F\'
            AND TL_iw_gsaen.Estado = \'V\'
            AND TL_iw_gsaen.Fecha > DATEADD(month, -12, GETDATE())
            
            GROUP BY MONTH(TL_iw_gsaen.Fecha), YEAR(TL_iw_gsaen.Fecha)
                        
            UNION ALL
            
            SELECT
                SUM(TS_iw_gsaen.Total) AS Total,
                MONTH(TS_iw_gsaen.Fecha) AS FechaMes,
                YEAR(TS_iw_gsaen.Fecha) AS FechaAno,
                \'TENSPA\' AS empresa
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            
            LEFT JOIN TENSPA.softland.cwtauxven AS TS_cwtauxven ON TS_iw_gsaen.CodAux = TS_cwtauxven.CodAux
            
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_cwtauxven.VenCod = TS_cwtvend.VenCod
            
            WHERE
                TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND TS_iw_gsaen.Tipo = \'F\'
            AND TS_iw_gsaen.Estado = \'V\'
            AND TS_iw_gsaen.Fecha > DATEADD(month, -12, GETDATE())
            
            GROUP BY MONTH(TS_iw_gsaen.Fecha), YEAR(TS_iw_gsaen.Fecha)
            
            ORDER BY FechaAno DESC, FechaMes DESC  
        '));

        return $sql;
    }

    public static function informeComercialGR($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
                
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod 
            
            WHERE
                GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualGR($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
                
            LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod 
            
            WHERE
                GR_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreGR($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (GR_iw_gsaen.subTotal) AS Total,
                        MONTH (GR_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (GR_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen
            
                    LEFT JOIN GRAULTDA.softland.cwtvend AS GR_cwtvend ON GR_iw_gsaen.CodVendedor = GR_cwtvend.VenCod 
            
                    WHERE GR_cwtvend.VenDes = \'' . $nombre . '\'
                    AND (GR_iw_gsaen.Tipo = \'F\' OR GR_iw_gsaen.Tipo = \'N\')
                    AND GR_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (GR_iw_gsaen.Fecha),
                        YEAR (GR_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialGS($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
                
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod 
            
            WHERE
                GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualGS($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
                
            LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod 
            
            WHERE
                GS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreGS($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (GS_iw_gsaen.subTotal) AS Total,
                        MONTH (GS_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (GS_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen
            
                    LEFT JOIN GRAUSPA.softland.cwtvend AS GS_cwtvend ON GS_iw_gsaen.CodVendedor = GS_cwtvend.VenCod 
            
                    WHERE GS_cwtvend.VenDes = \'' . $nombre . '\'
                    AND (GS_iw_gsaen.Tipo = \'F\' OR GS_iw_gsaen.Tipo = \'N\')
                    AND GS_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (GS_iw_gsaen.Fecha),
                        YEAR (GS_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialMB($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
                
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod 
            
            WHERE
                MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualMB($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
                
            LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod 
            
            WHERE
                MB_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreMB($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (MB_iw_gsaen.subTotal) AS Total,
                        MONTH (MB_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (MB_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        MICROBOX.softland.iw_gsaen AS MB_iw_gsaen
            
                    LEFT JOIN MICROBOX.softland.cwtvend AS MB_cwtvend ON MB_iw_gsaen.CodVendedor = MB_cwtvend.VenCod 
            
                    WHERE MB_cwtvend.VenDes = \'' . $nombre . '\'
                    AND (MB_iw_gsaen.Tipo = \'F\' OR MB_iw_gsaen.Tipo = \'N\')
                    AND MB_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (MB_iw_gsaen.Fecha),
                        YEAR (MB_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialTL($nombre) {

    $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
                
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod 
            
            WHERE
                TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

    return $sql;
}

    public static function totalAnualTL($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
                
            LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod 
            
            WHERE
                TL_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreTL($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (TL_iw_gsaen.subTotal) AS Total,
                        MONTH (TL_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (TL_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            
                    LEFT JOIN PUBLIGRAFIKA.softland.cwtvend AS TL_cwtvend ON TL_iw_gsaen.CodVendedor = TL_cwtvend.VenCod 
            
                    WHERE TL_cwtvend.VenDes = \'' . $nombre . '\'
                    AND (TL_iw_gsaen.Tipo = \'F\' OR TL_iw_gsaen.Tipo = \'N\')
                    AND TL_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (TL_iw_gsaen.Fecha),
                        YEAR (TL_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialTS($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
                
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod 
            
            WHERE
                TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualTS($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
                
            LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod 
            
            WHERE
                TS_cwtvend.VenDes = \'' . $nombre . '\'
            AND (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreTS($nombre) {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (TS_iw_gsaen.subTotal) AS Total,
                        MONTH (TS_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (TS_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            
                    LEFT JOIN TENSPA.softland.cwtvend AS TS_cwtvend ON TS_iw_gsaen.CodVendedor = TS_cwtvend.VenCod 
            
                    WHERE TS_cwtvend.VenDes = \'' . $nombre . '\'
                    AND (TS_iw_gsaen.Tipo = \'F\' OR TS_iw_gsaen.Tipo = \'N\')
                    AND TS_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (TS_iw_gsaen.Fecha),
                        YEAR (TS_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }    
}
