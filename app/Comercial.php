<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercial extends Model
{
    public static function informeComercialGR() {
        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen    
            WHERE
              (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualGR() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen  
            WHERE 
              (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreGR() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                GR_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (GR_iw_gsaen.subTotal) AS Total,
                        MONTH (GR_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (GR_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        GRAULTDA.softland.iw_gsaen AS GR_iw_gsaen            
                    WHERE
                      (GR_iw_gsaen.Tipo = \'F\' OR GR_iw_gsaen.Tipo = \'N\')
                    AND GR_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (GR_iw_gsaen.Fecha),
                        YEAR (GR_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialGS() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen            
            WHERE
             (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualGS() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreGS() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                GS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (GS_iw_gsaen.subTotal) AS Total,
                        MONTH (GS_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (GS_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        GRAUSPA.softland.iw_gsaen AS GS_iw_gsaen            
                    WHERE
                      (GS_iw_gsaen.Tipo = \'F\' OR GS_iw_gsaen.Tipo = \'N\')
                    AND GS_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (GS_iw_gsaen.Fecha),
                        YEAR (GS_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialMB() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen                          
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualMB() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                MICROBOX.softland.iw_gsaen AS MB_iw_gsaen            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreMB() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                MB_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (MB_iw_gsaen.subTotal) AS Total,
                        MONTH (MB_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (MB_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        MICROBOX.softland.iw_gsaen AS MB_iw_gsaen            
                    WHERE 
                      (MB_iw_gsaen.Tipo = \'F\' OR MB_iw_gsaen.Tipo = \'N\')
                    AND MB_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (MB_iw_gsaen.Fecha),
                        YEAR (MB_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialTL() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualTL() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreTL() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                TL_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (TL_iw_gsaen.subTotal) AS Total,
                        MONTH (TL_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (TL_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        PUBLIGRAFIKA.softland.iw_gsaen AS TL_iw_gsaen            
                    WHERE 
                      (TL_iw_gsaen.Tipo = \'F\' OR TL_iw_gsaen.Tipo = \'N\')
                    AND TL_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (TL_iw_gsaen.Fecha),
                        YEAR (TL_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }

    public static function informeComercialTS() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                MONTH (Fecha) AS FechaMes,
                YEAR (Fecha) AS FechaAno
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                MONTH (Fecha),
                YEAR (Fecha)
            ORDER BY FechaAno, FechaMes ASC        
        '));

        return $sql;
    }

    public static function totalAnualTS() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (subTotal) AS Total,
                YEAR (Fecha) AS FechaAno
            FROM
                TENSPA.softland.iw_gsaen AS TS_iw_gsaen            
            WHERE
                (Tipo = \'F\' OR Tipo = \'N\')
            AND Estado = \'V\'
            GROUP BY
                YEAR (Fecha)
            ORDER BY FechaAno ASC        
        '));

        return $sql;
    }

    public static function totalTrimestreTS() {

        $sql = \DB::select(\DB::raw('
            SELECT
                SUM (trimUno) as PrimerTrimestre,
                SUM (trimDos) as SegundoTrimestre,
                SUM (trimTres) as TercerTrimestre,
                SUM (trimCuatro) as CuartoTrimestre,
                trimestres.FechaAno
            FROM
                (
                    SELECT
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 1
                            AND 3 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimUno,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 4
                            AND 6 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimDos,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 7
                            AND 9 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimTres,
                        SUM (
                            CASE
                            WHEN MONTH (Fecha) BETWEEN 10
                            AND 12 THEN
                                TS_iw_gsaen.SubTotal
                            ELSE
                                0
                            END
                        ) AS trimCuatro,
                        SUM (TS_iw_gsaen.subTotal) AS Total,
                        MONTH (TS_iw_gsaen.Fecha) AS FechaMes,
                        YEAR (TS_iw_gsaen.Fecha) AS FechaAno
                    FROM
                        TENSPA.softland.iw_gsaen AS TS_iw_gsaen
            
            
                    WHERE
                      (TS_iw_gsaen.Tipo = \'F\' OR TS_iw_gsaen.Tipo = \'N\')
                    AND TS_iw_gsaen.Estado = \'V\'
                    GROUP BY
                        MONTH (TS_iw_gsaen.Fecha),
                        YEAR (TS_iw_gsaen.Fecha)
                ) AS trimestres
            GROUP BY
                trimestres.FechaAno
            ORDER BY trimestres.FechaAno ASC                        
        '));

        return $sql;
    }
}
