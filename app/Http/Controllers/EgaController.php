<?php

namespace App\Http\Controllers;

use App\Ega;
use Illuminate\Http\Request;

class EgaController extends Controller
{
    public function index() {

        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') . " " . date("G:i:s");
        $ano = date('Y');
        $mes = date('n');

        return view('ega.informe', [
            'meses' => $meses,
            'fecha' => $fecha,
            'ano' => $ano,
            'mes' => $mes,
            'totalActualEmpresas' => Ega::totalActualEmpresas(),
            'facturasPorCobrar' => Ega::totalVencidoYporVencerEmpresas(),
            'totalFactoring' => Ega::totalOperacionesFactoring()
        ]);
    }
}
