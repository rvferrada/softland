<?php

namespace App\Http\Controllers;

use App\Comercial;
use Illuminate\Http\Request;

class ComercialController extends Controller
{
    public function index() {

        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
        $meses = array(1 => "Enero",2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");
        $fecha = $dias[date('w')]." ".date('d')." de ".$meses[date('n')]. " del ".date('Y') . " " . date("G:i:s");

        $anoComienzo = date('Y')-4; //Mostrara solamente los últimos 5 años en tabla.
        $anos = date('Y') - $anoComienzo;

        return view('comercial.informe', [
            'anoActual' => date('Y'),
            'anoComienzo' => $anoComienzo,
            'anos' => $anos,
            'fecha' => $fecha,
            'meses' => $meses,
            /* GRAULTDA */
            'informeComercialGR' => Comercial::informeComercialGR(),
            'totalAnualGR' => Comercial::totalAnualGR(),
            'totalTrimestreGR' => Comercial::totalTrimestreGR(),
            /* GRAUSPA */
            'informeComercialGS' => Comercial::informeComercialGS(),
            'totalAnualGS' => Comercial::totalAnualGS(),
            'totalTrimestreGS' => Comercial::totalTrimestreGS(),
            /* MICROBOX */
            'informeComercialMB' => Comercial::informeComercialMB(),
            'totalAnualMB' => Comercial::totalAnualMB(),
            'totalTrimestreMB' => Comercial::totalTrimestreMB(),
            /* TENDENCIA LTDA */
            'informeComercialTL' => Comercial::informeComercialTL(),
            'totalAnualTL' => Comercial::totalAnualTL(),
            'totalTrimestreTL' => Comercial::totalTrimestreTL(),
            /* TENDENCIA SPA */
            'informeComercialTS' => Comercial::informeComercialTS(),
            'totalAnualTS' => Comercial::totalAnualTS(),
            'totalTrimestreTS' => Comercial::totalTrimestreTS()            
        ]);
    }
}
