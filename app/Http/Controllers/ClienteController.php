<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Factura;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
    public function index(Request $request) {

        $this->validate($request, [
            'rut' => 'required|numeric',
            'nombre' => 'required',
        ]);

        if (!$this->comprobarRutYnombreCliente($request->get('nombre'), $request->get('rut'))) {
            return redirect('/')->withErrors('El rut o nombre del cliente ingresado es incorrecto.');
        }

        return $this->generarInformeDeCliente($request->get('nombre'), $request->get('rut'));
    }

    public function comprobarRutYnombreCliente($nombre, $rut) {

        //Consultamos las diferentes base de datos de empresas para las sugerencias.
        if (Cliente::comprobarRutYnombreClienteGrauLtda($nombre, $rut)) {
            return true;

        } else if (Cliente::comprobarRutYnombreClienteGrauSpa($nombre, $rut)) {
            return true;

        } else if (Cliente::comprobarRutYnombreClienteMicrobox($nombre, $rut)) {
            return true;

        } else if (Cliente::comprobarRutYnombreClienteTendenciaLtda($nombre, $rut)) {
            return true;

        } else if (Cliente::comprobarRutYnombreClienteTendenciaSpa($nombre, $rut)) {
            return true;

        }
        return false;
    }

    public function generarInformeDeCliente($nombre, $rut) {

        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
        $meses = array("Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre");
        $fecha = $dias[date('w')]." ".date('d')." de ".$meses[date('n')-1]. " del ".date('Y') . " " . date("G:i:s");
        $ano = date('Y');

        $nombre = mb_strtoupper(mb_strtolower($nombre));

        return view('cliente.informe', [
            'fecha' => $fecha,
            'ano' => $ano,
            'nombre' => $nombre,
            'rut' => $rut,
            'totalAnoActual' => Factura::totalAnoActual($rut),
            'totalAnoAnterior' => Factura::totalAnoAnterior($rut),
            'totalVencidoYporVencer' => Factura::totalVencidoYporVencer($rut), //total vencido, total por vencer.
            'facturasEnFactoring' => Factura::facturasEnFactoring($rut),
            'facturasMasVencidasPorVencer' => Factura::facturasMasVencidasOporVencer($rut, 20), //Mostrar solo 2.
            'facturasMasVencidasPorVencerMostrar' => 2,
            'condicionesVenta' => Cliente::condicionesVenta($rut),
            'promedioPago' => Factura::promedioPago($rut, 5),
            'productosDeCliente' => Cliente::productosDeCliente($nombre)
        ]);
    }

    public function buscador() {

        return view('cliente.buscador')->with('info', 'Puede generar un informe ingresando el rut o nombre del cliente en el formulario.');;
    }
}
