<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Vendedor;

class VendedorController extends Controller
{
    public function index(Request $request) {

        $this->validate($request, [
            'nomVend' => 'required'
        ]);

        if (!$this->comprobarNombreVendedor($request->get('nomVend'))) {
            return redirect('vendedores')->withErrors('El nombre del vendedor ingresado es incorrecto.');
        }

        return $this->generarInformeDeVendedor($request->get('nomVend'));
    }

    public function generarInformeDeVendedor($nombre) {

        $dias = array("Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado");
        $meses = array(1 => "Enero",2 => "Febrero", 3 => "Marzo", 4 => "Abril", 5 => "Mayo", 6 => "Junio", 7 => "Julio", 8 => "Agosto", 9 => "Septiembre", 10 => "Octubre", 11 => "Noviembre", 12 => "Diciembre");
        $fecha = $dias[date('w')]." ".date('d')." de ".$meses[date('n')]. " del ".date('Y') . " " . date("G:i:s");

        $nombre = mb_strtoupper(mb_strtolower($nombre));

        $anoComienzo = date('Y')-4; //Mostrara solamente los últimos 5 años en tabla.
        $anos = date('Y') - $anoComienzo;

        return view('vendedor.informe', [
            'anoActual' => date('Y'),
            'anoComienzo' => $anoComienzo,
            'anos' => $anos,
            'fecha' => $fecha,
            'meses' => $meses,
            'nombre' => $nombre,
            'totalFacturadoHaceDosMeses' =>  Vendedor::totalFacturadoHaceDosMeses($nombre),
            'totalFacturadoHaceUnMes' => Vendedor::totalFacturadoHaceUnMes($nombre),
            'totalFacturadoActual' => Vendedor::totalFacturadoActual($nombre),
            'totalCobradoHaceUnMes' => Vendedor::totalCobradoHaceUnMes($nombre),
            'totalCobradoMesActual' => Vendedor::totalCobradoMesActual($nombre),
            'totalVencidoYporVencer' => Vendedor::totalVencidoYporVencer($nombre),
            /*'totalUltimosDoceMeses' => Vendedor::totalUltimosDoceMeses($nombre),*/ //cambios 22-02-2017
            /* GRAULTDA */
            'informeComercialGR' => Vendedor::informeComercialGR($nombre),
            'totalAnualGR' => Vendedor::totalAnualGR($nombre),
            'totalTrimestreGR' => Vendedor::totalTrimestreGR($nombre),
            /* GRAUSPA */
            'informeComercialGS' => Vendedor::informeComercialGS($nombre),
            'totalAnualGS' => Vendedor::totalAnualGS($nombre),
            'totalTrimestreGS' => Vendedor::totalTrimestreGS($nombre),
            /* MICROBOX */
            'informeComercialMB' => Vendedor::informeComercialMB($nombre),
            'totalAnualMB' => Vendedor::totalAnualMB($nombre),
            'totalTrimestreMB' => Vendedor::totalTrimestreMB($nombre),
            /* TENDENCIA LTDA */
            'informeComercialTL' => Vendedor::informeComercialTL($nombre),
            'totalAnualTL' => Vendedor::totalAnualTL($nombre),
            'totalTrimestreTL' => Vendedor::totalTrimestreTL($nombre),
            /* TENDENCIA SPA */
            'informeComercialTS' => Vendedor::informeComercialTS($nombre),
            'totalAnualTS' => Vendedor::totalAnualTS($nombre),
            'totalTrimestreTS' => Vendedor::totalTrimestreTS($nombre)
        ]);
    }

    public function comprobarNombreVendedor($nombre) {

        if (Vendedor::comprobarNombreVendedorEmpresas($nombre)) {
            return true;
        }

        return false;
    }

    public function buscador() {

        return view('vendedor.buscador', [
            'vendedores' => Vendedor::nombreDeVendedores()
        ]);
    }

    public static function getTablaMes($mes) {

    }
}
