<?php

namespace App\Http\Controllers;

use App\Buscador;
use Illuminate\Http\Request;

class BuscadorController extends Controller
{
    public function nombre(Request $request) {

        if($request->ajax()) {

            //Recibimos la búsqueda realizada. /b/nombre?query=
            $nombre = $request->get('query');
            $top = 15;

            //Consultamos las diferentes base de datos de empresas para las sugerencias.
            if (count(Buscador::getNombresGrauLtda($nombre, $top)) > 0) {
                $dataValue = Buscador::getNombresGrauLtda($nombre, $top);

            } else if (count(Buscador::getNombresGrauSpa($nombre, $top)) > 0) {
                $dataValue = Buscador::getNombresGrauSpa($nombre, $top);

            } else if (count(Buscador::getNombresMicrobox($nombre, $top)) > 0) {
                $dataValue = Buscador::getNombresMicrobox($nombre, $top);

            } else if (count(Buscador::getNombresTendenciaLtda($nombre, $top)) > 0) {
                $dataValue = Buscador::getNombresTendenciaLtda($nombre, $top);

            } else if (count(Buscador::getNombresTendenciaSpa($nombre, $top)) > 0) {
                $dataValue = Buscador::getNombresTendenciaSpa($nombre, $top);

            } else {
                $dataValue = [];
            }

            return response()->json([
                'query' => 'unit',
                'suggestions' => $dataValue
            ]);

        } else {
            abort(404);
        }
    }

    public function rut(Request $request) {

        if($request->ajax()) {

            //Recibimos la búsqueda realizada. /b/rut?query=
            $rut = $request->get('query');
            $top = 15;

            //Consultamos las diferentes base de datos de empresas para las sugerencias.
            if (count(Buscador::getRutGrauLtda($rut, $top)) > 0) {
                $dataValue = Buscador::getRutGrauLtda($rut, $top);

            } else if (count(Buscador::getRutGrauSpa($rut, $top)) > 0) {
                $dataValue = Buscador::getRutGrauSpa($rut, $top);

            } else if (count(Buscador::getRutMicrobox($rut, $top)) > 0) {
                $dataValue = Buscador::getRutMicrobox($rut, $top);

            } else if (count(Buscador::getRutTendenciaLtda($rut, $top)) > 0) {
                $dataValue = Buscador::getRutTendenciaLtda($rut, $top);

            } else if (count(Buscador::getRutTendenciaSpa($rut, $top)) > 0) {
                $dataValue = Buscador::getRutTendenciaSpa($rut, $top);

            } else {
                $dataValue = [];
            }

            return response()->json([
                'query' => 'unit',
                'suggestions' => $dataValue
            ]);

        } else {
            abort(404);
        }
    }
}
